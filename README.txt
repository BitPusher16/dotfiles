$ ssh-keygen -t rsa -b 4096 -C "jglenn@somedomain.com"
$ ssh-add ~/.ssh/id_rsa
$ xclip -sel clip < ~/.ssh/id_rsa.pub # (assumes xclip is installed)
$ # (add public key to bitbucket)
$ ssh -T git@bitbucket.org # (test ssh)
$ cd ~/src
$ git clone git@bitbucket.org:BitPusher16/dotfiles.git # don't clone with http
$ cd ~
$ ln -s ~/src/dotfiles/dot ~/dot

I have tried several schemes for quickly installing my dotfiles on a fresh os.

My first attempt was to use a script to backup the system's default dotfiles
and replace them with dynamic links to the dotfiles in my repo.
However, this caused me to lose all settings in the default system dotfiles.

My next attempt was to use a script to append the contents of my dotfiles
to the system dotfiles.
This was a poor approach because any changes I made to the dotfiles after
appending could not be pushed back to my master dotfile repo.

Next, I tried using a script to insert a single include statement
into each system dotfile.
This include statement grabs the contents of my repo dotfiles.
This worked well, but changed too many things at once.

Current approach is to clone the repo and create a symbolic link
from home dir to config dir.
Then, for any personalized config file I want to activate,
I put the appropriate "source" command in the existing home dir config file.

2024-03-03
Some thoughts. 
If I want to simplify installation, I can make repo public and 
simply clone without auth. I can set up auth later if I want to edit dotfiles.
Different programs may have different config methods:
some programs may have dotfiles already created and populated on a new machine,
while others will have no dotfiles.
A persisting problem here is that I wish to keep all my configs in a repo,
but some programs expect those config files to appear at fixed locations
in file system.

