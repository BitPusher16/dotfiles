# to include in ~/.bashrc, add this line:
#source ~/mc/.bashrc

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
PS1='$(whoami):$(hostname):$(pwd)$ '

set bell-style none
# next line doesn't do what i expect...
#set show-all-if-ambiguous on'
bind 'set show-all-if-ambiguous on'
#bind 'TAB:menu-complete'
bind 'set completion-ignore-case on'
alias la='ls -lahpL'
alias hs='history 20'
alias pa='echo $PATH | tr ":" "\n"'
alias en='printenv'
alias cs='clear'
# better to use fc than history?
alias vim='nvim'
cl(){
	cd $1
	ls -lahp
}

# prevent Ctrl-s from calling flow control method XOFF, freezing the terminal;
# (if wish to omit this, can use Ctrl-q to resume after Ctrl-s)
stty -ixon

# enable vim command line editing
set -o vi

# prevent clobber
set -o noclobber

# Prevent bash from escaping '$' by prepending '\'.
shopt -s direxpand

# Start tmux on login.
# https://unix.stackexchange.com/questions/43601/how-can-i-set-my-default-shell-to-start-up-tmux
if command -v tmux &> /dev/null && [ -n "$PS1" ] && [[ ! "$TERM" =~ screen ]] && [[ ! "$TERM" =~ tmux ]] && [ -z "$TMUX" ]; then
	
  tmux new-session -d -x "$(tput cols)" -y "$(tput lines)"

  # https://stackoverflow.com/a/40009032/2512141
  # https://unix.stackexchange.com/a/569731
  #tmux new-session -d -x "$(tput cols)" -y "$(tput lines)"
  #tmux split-window -h -p 80
  #tmux split-window -t 0 -v
  #tmux send-keys -t 0 "htop" ENTER
  #tmux send-keys -t 1 "df -h" ENTER
  #tmux select-pane -t 2

  exec tmux -2 attach-session -d

fi

# set up some tmux windows
#tmux split-window -v \;

source $SCRIPT_DIR/.binpaths
