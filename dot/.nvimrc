set colorcolumn=80

" use colors from the terminal?
" see this link:
" https://github.com/neovim/neovim/issues/2334#issuecomment-330057651
" seems that termguicolors enables 24-bit color, which conflicts with tmux?
"set termguicolors " has opposite of expected effect.
"set notermguicolors " uses same color scheme as terminal. (??)
"set notermguicolors 
"set t_Co=16
"set t_Co=8

" set a color scheme which uses only the 16 system colors.
" this way, we don't have to set a specific color scheme for vim,
" it will inherit the scheme set for the shell.
" use these colorscheme files:
" https://github.com/jeffkreeftmeijer/vim-dim/tree/main/colors

":so dim.vim
":so ./grim.vim

" a bit of vimscript magic to get the dir of this script.
" https://stackoverflow.com/questions/4976776/how-to-get-path-to-the-current-vimscript-being-executed

"let s:script_dir = fnamemodify(resolve(expand('<sfile>:p')), ':h')
"exec 'source' s:script_dir . '/dim.vim'
":so s:script_dir . '/dim.vim'

"exec "source " . expand('<sfile>:p:h') . "/dim.vim"

"let s:script_dir = fnamemodify(resolve(expand('<sfile>:p')), ':h')
"let s:dim_script = s:script_dir . '/dim.vim'
"execute "source " . s:dim_script

":so ~/mc/dim.vim

" found a way to add a directory with my own vim colorschemes.
" https://stackoverflow.com/questions/45911711/vim-how-to-set-directory-used-for-colorschemes
" directory must contain subdir /colors

let s:script_dir = fnamemodify(resolve(expand('<sfile>:p')), ':h')
let s:color_dir = s:script_dir . '/vimcolors'
execute "set runtimepath+=" . s:color_dir
colorscheme dim



"call plug#begin('~/.config/nvim/autoload/plug.vim')
  "Plug 'scrooloose/nerdtree', { 'on': 'NERDTreeToggle' }
  "Plug 'jiangmiao/auto-pairs'
  "Plug 'preservim/nerdtree'
"call plug#end()
