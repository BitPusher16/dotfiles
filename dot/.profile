
# LESS specifies the options that will be passed to less.
# https://unix.stackexchange.com/questions/269828/what-is-the-less-environment-variable-to-man-and-how-is-it-set-up
# this link says the line is needed in .profile to disable bell in less:
# https://stackoverflow.com/questions/36724209/disable-beep-in-wsl-terminal-on-windows-10
export LESS="$LESS -R -Q"

