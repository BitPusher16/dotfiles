#pragma once

#include <iostream>
#include <array>
#include <vector>
#include <memory>

using std::cout;
using std::endl;
using std::array;
using std::vector;
using std::unique_ptr;


class point_2d
{
public:
	int x;
	int y;
	//point_2d(int a, int b) :x(a), y(b) {};
};

class square_2d
{
public:
	point_2d top_left;
	point_2d bot_right;
};

class square_2d_ptr_members
{
public:
	point_2d* top_left;
	point_2d* bot_right;

	~square_2d_ptr_members()
	{
		delete top_left;
		delete bot_right;
	}

};

void pass_int_stack(int x) { cout << x << endl; }
int return_int_stack() { return 3; }

void pass_int_heap(int* x) { cout << *x << endl; }
int* return_int_heap() { int* ret = new int; *ret = 3; return ret; }

void pass_class_stack(point_2d p) { cout << p.x << endl; }
point_2d return_class_stack() { point_2d ret; ret.x = 8; ret.y = 9; return ret; }

void pass_class_heap(point_2d* p) { cout << p->x << endl; }
point_2d* return_class_heap() { point_2d* ret = new point_2d; ret->x = 1; ret->y = 2; return ret; }

// CAUTION!! I previously thought that containers (such as vector<>) were simply structs containing pointers to underlying data structures.
// I assumed that methods called on a container passed in by copy would affect the container in caller space. (Through pointers.)
// This was wrong!! Calling push_back() on the function's copy of the vector does not affect caller's vector.
// So does that mean vectors passed by copy are deep copied, data and all? Interesting... That would have serious performance repercussions.
// Also see https://stackoverflow.com/questions/11348376/std-vector-c-deep-or-shallow-copy.
// "You are making a deep copy any time you copy a vector. But if your vector is a vector of pointers you are getting
// the copy of pointers, not the values pointed to."
// Update: Or, a container actually does contain pointers (and this allows returning vector from function without copying everything?),
// but copy constructor is called if passed to function thoughtlessly?

// Also, note that container subscript operator [] returns a reference, but copying that reference to non-ref will make a copy.
// https://stackoverflow.com/questions/19608535/passing-a-vector-by-reference-to-function-but-change-doesnt-persist

void pass_container_stack(vector<int> v) { cout << v[0] << endl; v.push_back(44); cout << v[1] << endl; }
vector<int> return_container_stack() { vector<int> ret; ret.push_back(13); return ret; }

void pass_container_heap(vector<int>* v) { cout << (*v)[0] << endl; v->push_back(45); }
vector<int>* return_container_heap() { vector<int>* ret = new vector<int>; ret->push_back(21); return ret; }

void pass_arr_stack(int* x) { cout << x[0] << endl; }

void pass_arr_heap(int* x) { cout << x[0] << endl; x[1] = 7; }
int* return_arr_heap() { int* ret = new int[4]; ret[0] = 9; return ret; }

int& return_lref(int& x) { return x; }


// Better to move this inside scalar{} class?
//static int num_scalars;

class scalar
{
public:

	// This cannot be set inside any constructor. It must be set outside the class.
	static int num_scalars;

	// Const members can only be set in initializer list.
	const int id;

	int x;

	//scalar(int arg_x) { id = num_scalars++; x = arg_x; } // error, can't set const inside code block
	//scalar(int arg_x):id(num_scalars++) { x = arg_x; } // mixed
	//scalar(int x) :id(num_scalars++), x(x) {} // initializer list

	// constructor
	scalar() :id(num_scalars++) { cout << "scalar " << id << " constructor" << endl; }

	scalar(int ax) :id(num_scalars++) { cout << "scalar " << id << " 1-int constructor" << endl; x = ax; };

	// destructor
	~scalar() { cout << "scalar " << id << " destructor" << endl; }

	// copy constructor
	scalar(const scalar& s) :id(num_scalars++) { cout << "scalar " << id << " copy constructor" << endl; x = s.x; }

	// copy assignment
	scalar& operator=(const scalar& s) { cout << "scalar " << id << " copy assignment" << endl; x = s.x; return *this; }

	// move constructor (class has no pointer members, so this would normally not be needed?)
	// see https://stackoverflow.com/questions/22048644/move-semantics-for-pod-ish-types, move semantics for POD types.
	scalar(scalar&& s) :id(num_scalars++) { cout << "scalar " << id << " move constructor" << endl; x = s.x; s.x = 0; }

	// move assignment (class has no pointer members, so this would normally not be needed?)
	scalar& operator=(scalar&& s) { cout << "scalar " << id << " move assignment" << endl; x = s.x; s.x = 0; return *this; }
};
// Initialize scalar class's static variable.
int scalar::num_scalars = 0;

class complex
{
public:
	static int num_complex;
	const int id;
	scalar real;
	scalar imag;

	// constructor
	complex() :id(num_complex++) { cout << "complex " << id << " constructor" << endl; }
	complex(int real, int imag) :id(num_complex++), real(real), imag(imag) { cout << "complex " << id << " 2-int constructor" << endl; }
	complex(scalar real, scalar imag) :id(num_complex++), real(real), imag(imag) { cout << "complex " << id << " 2-scalar constructor" << endl; }
	// destructor
	~complex() { cout << "complex " << id << " destructor" << endl; }
	// copy constructor
	complex(const complex& s) :id(num_complex++), real(s.real), imag(s.imag) { cout << "complex " << id << " copy constructor" << endl; }
	// copy assignment
	complex& operator=(const complex& s) { real = s.real; imag = s.imag; cout << "complex " << id << " copy assignment" << endl; return *this; }
	// move constructor
	complex(complex&& s) :id(num_complex++) { real = s.real; imag = s.imag; cout << "complex " << id << " move constructor" << endl; }
	// move assignment
	complex& operator=(complex&& s) { real = s.real; imag = s.imag; cout << "complex " << id << " move assignment" << endl; return *this; }
};
int complex::num_complex = 0;


// This class uses slightly nonstandard initialization techniques so that printing strings can happen before xtor internals are called.
class complex_ptr_members
{
public:
	static int num_complex_pm;
	const int id;
	scalar* real;
	scalar* imag;

	// constructor
	complex_ptr_members() :id(num_complex_pm++) { cout << "complex_ptr_members " << id << " 0-param constructor" << endl; 
		real = new scalar(0); imag = new scalar(0);}
	complex_ptr_members(int a_real, int a_imag) :id(num_complex_pm++) { cout << "complex_ptr_members " << id << " 2-int constructor" << endl;
		real = new scalar(a_real); imag = new scalar(a_imag);}
	complex_ptr_members(scalar* a_real, scalar* a_imag) :id(num_complex_pm++), real(a_real), imag(a_imag){ 
		cout << "complex_ptr_members " << id << " 2-scalar constructor" << endl; }

	// destructor
	~complex_ptr_members() { cout << "complex_ptr_members " << id << " destructor" << endl; delete real; delete imag; }

	// copy constructor
	complex_ptr_members(const complex_ptr_members& s) :id(num_complex_pm++) { 
		cout << "complex_ptr_members " << id << " copy constructor" << endl; real = new scalar(s.real->x); imag = new scalar(s.imag->x);}
	// copy assignment
	complex_ptr_members& operator=(const complex_ptr_members& s) { cout << "complex_ptr_members " << id << " copy assignment" << endl;
		 delete real; delete imag; real = new scalar(s.real->x); imag = new scalar(s.imag->x); return *this; }
	// move constructor
	complex_ptr_members(complex_ptr_members&& s) :id(num_complex_pm++) { cout << "complex_ptr_members " << id << " move constructor" << endl;
		 real = s.real; s.real = nullptr; imag = s.imag; s.imag = nullptr;}
	// move assignment
	complex_ptr_members& operator=(complex_ptr_members&& s) { cout << "complex_ptr_members " << id << " move assignment" << endl;
		real = s.real; s.real = nullptr; imag = s.imag; s.imag = nullptr; return *this; }
};
int complex_ptr_members::num_complex_pm = 0;

void print_two_scalars(array<scalar, 2> a)
{
	for (int i = 0; i < a.size(); i++) { cout << a[i].x << endl; }
}

void print_two_scalars_ref(array<scalar, 2>& a)
{
	for (int i = 0; i < a.size(); i++) { cout << a[i].x << endl; }
}

array<scalar, 2> return_two_scalars()
{
	array<scalar, 2> ret;
	ret[0].x = 17;
	ret[1].x = 19;
	return ret;
}

array<scalar, 2>& return_two_scalars_ref()
{
	array<scalar, 2> ret;
	ret[0].x = 21;
	ret[1].x = 22;
	return ret;
}


// Example of using RVO to return a vector without copying everything?
vector<int> return_fours(int n)
{
	vector<int> ret;
	for (int i = 0; i < n; i++)
	{
		ret.push_back(4);
	}
	return ret;
}

//int baz(int x) { return x + 3; } // This function causes overload conflict with both baz(int& x) and baz(int&& x).
int baz(int& x) { return x + 1; }
int baz(int&& x) { return x + 2; }

int& get_indexed_location(int a[], int idx)
{
	return a[idx]; // Note: Since we have declared return type of lvalue reference, there is an implicit & here.

	// Alternatively:
	//int& ret = a[idx]; // Note: & is implicit.
	//return ret;
}