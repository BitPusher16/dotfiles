#include "single.hpp"

// behaviors I need to read up on:
// when smart pointer goes out of scope, it calls delete?
// when calling delete on an object, that object's destructor is called?

// 1) "A destructor is a member function that is invoked automatically when the object goes out of scope..." 
//    (Would only be helpful for auto-deallocating on-stack objects. On-heap objects can't deallocate themselves.)
//    https://docs.microsoft.com/en-us/cpp/cpp/destructors-cpp?view=vs-2019
// 2) "When delete is used to deallocate memory for a C++ class object, the object's destructor is called before the object's memory is deallocated (if the object has a destructor)."
//    https://docs.microsoft.com/en-us/cpp/cpp/delete-operator-cpp?view=vs-2019
//    (Use this to manually deallocate on-heap objects.)
// 3) "The smart pointer destructor contains the call to delete, and because the smart pointer is declared on the stack, 
//    its destructor is invoked when the smart pointer goes out of scope, even if an exception is thrown somewhere further up the stack."
//    https://docs.microsoft.com/en-us/cpp/cpp/smart-pointers-modern-cpp?view=vs-2019
//    Is it always the case that smart pointers are declared on the stack?

// So if a class uses smart pointers for its child members, when that object is deallocated, it will automatically clean up its children?

// "Elements are copied into a container."
// Tour of C++, 2nd. p.197

// Writing capable programs only needs a few basic features...
// I need a way to declare objects on the stack that will be cleaned up on scope exit. I need to be able to pass these to functions and modify them.
// I need a way to declare objects on the heap that will be cleaned up, or returned to a caller. Can I always use Containers for this to automate cleanup?
// Also, if I use smart pointers in my classes, they will cascade deallocate.
// Can I do everything I need without using new/delete?

// One way to guarantee deallocation is to never allocate memory and return to a caller. 
// As program flow dips into and out of call stack, allocated memory only exists at the caller's stack level and below.
// Lowever-level functions can modify objects passed to them, but not allocate new memory.
// (We can relax this slightly by allowing called functions to insert items into containers, which are auto cleaned?)

// How can we safely accommodate factory functions?
// Do we need factory functions? Why not always use constructors?
// Answer: Factories can construct one of several objects based on a passed parameter.
// https://stackoverflow.com/questions/628950/constructors-vs-factory-methods/629216
// Can we force factories to always return smart pointers?


int main(int argc, char* argv[])
{
	cout << "hello" << endl;

	/**********
	Stack vs heap.
	**********/

	// Note the symmetries below between int, class, and container.
	// Note that array has slightly different constraints.
	// Also note that the C++ array declaration syntax "int foo[]" differs from the C# and Java syntax: "int[] foo".

	// Int on stack. Declare, assign, pass, return.

	int int_stack;
	int_stack = 3;
	pass_int_stack(int_stack);
	int int_stack_ret = return_int_stack();

	// Int on heap. Declare, assign, pass, return.

	int* int_heap = new int;
	*int_heap = 6;
	pass_int_heap(int_heap);
	delete int_heap;
	int* ptr_int_ret = return_int_heap();
	delete ptr_int_ret;

	// Class/struct on stack. Declare, assign, pass, return.

	point_2d class_stack;
	class_stack.x = 3;
	class_stack.y = 4;
	pass_class_stack(class_stack);
	point_2d class_stack_ret = return_class_stack();

	// Class/struct on heap. Declare, assign, pass, return.

	point_2d* class_heap = new point_2d;
	class_heap->x = 5;
	class_heap->y = 6;
	pass_class_heap(class_heap);
	delete class_heap;
	point_2d* class_heap_ret = return_class_heap();
	delete class_heap_ret;

	// Container on stack. Declare, assign, pass, return.

	vector<int> container_stack;
	container_stack.push_back(5);
	pass_container_stack(container_stack);
	cout << container_stack[0] << endl;
	//cout << container_stack[1] << endl; // Throws an error. Function modified its copy, not our copy.
	vector<int> container_stack_ret = return_container_stack();

	// Container on heap. Declare, assign, pass, return.

	vector<int>* container_heap = new vector<int>;
	container_heap->push_back(17);
	pass_container_heap(container_heap);
	cout << (*container_heap)[1] << endl;
	delete container_heap;
	vector<int>* container_heap_ret = return_container_heap();
	delete container_heap_ret;

	// Array on stack. Declare, assign, pass, return.

	int arr_stack[4];
	arr_stack[0] = 7;
	pass_arr_stack(arr_stack);
	//int arr_stack_ret[] = return_arr_stack():
	// No way to receive an array returned from function's stack
	// because we don't know at compile time how large returned array would be.

	// Array on heap. Declare, assign, pass, return.

	int* arr_heap = new int[4];
	arr_heap[0] = 8;
	pass_arr_heap(arr_heap);
	delete[] arr_heap;
	int* arr_int_ret = return_arr_heap();
	delete[] arr_int_ret;

	// Alternative array declarations.

	int arr_stack2[]{1, 2, 3, 4};
	int arr_stack3[] = { 1, 2, 3, 4 };

	int* arr_heap2{ new int[4] };
	int* arr_heap3 = { new int[4] };



	/**********
	Classes.
	**********/

	// Better to use pointer members or reference members?
	// A: Use reference members if and only if you want to force a member object to exist before the parent object.
	// Remember, references are initialized once and cannot be reassigned.
	// https://stackoverflow.com/a/892361/2512141

	// All the syntaxes we might conceivably use to instantiate an object.
	// Note that point_2d has no explicit constructors.
	// An explicit constructor will change some of these behaviors.
	// Also, some of these work when a 0-param explicit constructor exists but break when only a non-0-param explicit constructor exists.

	point_2d p1;					// p.x and p.y are junk values.
	p1.x = 3;
	p1.y = 5;

	point_2d p2();					// Function declaration, no object instantiated.
	//point_2d p3(1, 2);			// compiler error: no suitable constructor exists to convert from int to point_2d. Works if point_2d has a 2-param constructor.
	point_2d p4(p1);				// Copies members of p1 into members of p2. If we had not initialized members of p1, this would be a compiler error.
	point_2d p5(point_2d p);		// Function declaration, no object instantiated.
	point_2d p6(point_2d p());		// Function declaration, no object instantiated.
	//point_2d p7(point_2d p{});	// compiler error.

	point_2d p8{};					// x, y are set to 0.
	point_2d p9{ 1, 2 };			// Sets x, y to 1, 2. Breaks if point_2d has an explicit 0-param constructor and no 2-param constructor.
	point_2d p10{ p1 };
	//point_2d p11{ point_2d p };	// compiler error.
	//point_2d p12{ point_2d p() }; // compiler error.
	//point_2d p13{ point_2d p{} }; // compiler error.

	//point_2d p14 = 3, 4;			// compiler error.
	//point_2d p15 = (3, 4);		// compiler error.
	point_2d p16 = { 3, 4 };		// initializer list.

	//point_2d p17 = point_2d;		// error
	point_2d p18 = point_2d();		// x, y are set to 0.
	point_2d p19 = point_2d{};		// x, y are set to 0.

	point_2d* p20 = new point_2d;	// x, y are junk values.
	point_2d* p21 = new point_2d(); // x, y are set to 0.
	point_2d* p22 = new point_2d{}; // x, y are set to 0.

	//delete p20, p21, p22; // Wrong!
	delete p20;
	delete p21;
	delete p22;


	// Class with explicit constructors, destructors, assignment.

	{
		scalar s0;			// Calls constructor.
		scalar sF();		// Function declaration. No object instantiated.
		scalar s1{};		// Calls constructor.

		scalar s2 = 8;		// Calls 1-param constructor. This syntax only works because scalar has a single member.
		scalar s3(8);		// Calls 1-param constructor. Won't work if we have an explicit 0-param ctor defined but no 1-param ctor defined.
		scalar s4{ 8 };		// Calls 1-param constructor. Won't work if we have an explicit 0-param ctor defined but no 1-param ctor defined.

		scalar s5 = s0;		// Calls copy constructor.
		scalar s6(s0);		// Calls copy constructor.
		scalar s7{ s0 };	// Calls copy constructor.

		// These call move constructors. Note that moved scalar could be in invalid state after move.
		scalar s8 = std::move(s0);
		scalar s9(std::move(s0));
		scalar s10{ std::move(s0) };

		scalar s11 = scalar();				// Calls constructor only, not constructor->copy-constructor->destructor. Return Value Optimization (RVO).
		scalar s13 = std::move(scalar());	// Calls constructor, then copy constructor, then destructor. Note we've skipped a number because extra object is created.

		scalar s14{ scalar{} };				// Calls constructor only. Return Value Optimization (RVO).
		scalar s16{ std::move(scalar{}) };	// Calls constructor->move-constructor->destructor. Note we've skipped a number because extra object is created.

		// Note that in the examples with std::move(), calling std::move() prevents Return Value Optimization.
		// https://www.internalpointers.com/post/c-rvalue-references-and-move-semantics-beginners, "I tried your code"

	} // Set a break point here to pause before destructors are called.

	// Composing a class on the stack.

	complex cx0;
	scalar sc0, sc1;
	sc0.x = 8;
	sc1.x = 9;
	cx0.real = sc0; // Copy assignment.
	cx0.imag = sc1; // Copy assignment.
	cout << cx0.real.x << endl;

	// Alternative notation.
	// Inefficient. Calls constructor, copy constructor, and destructor for both scalars.
	complex cx1{ scalar{3}, scalar{4} };

	// Inefficient, same as previous construction.
	complex cx2 = { scalar{5}, scalar{6} };

	// CAUTION. Function declaration for a function called cx3()
	// which returns a complex and expects pointers to two functions which return scalar.
	// https://stackoverflow.com/questions/1051379/is-there-a-difference-between-copy-initialization-and-direct-initialization
	complex cx3(scalar(), scalar());
	//cout << cx3.real.x << endl; // Compiler error. complex: cs3(scalar(*)(), scalar(*)());

	// Composing a class on the stack that has pointer members.

	cout << "=====" << endl;
	complex_ptr_members cx4{ new scalar{18}, new scalar{19} };
	// cx4 is cleaned up on scope exit. However, if destructor does not clean up members, we will have a memory leak.

	// Composing a  on the heap.

	// As with class on stack, we could also allocate a complex and then populate real, imag later.
	cout << "=====" << endl;
	complex* cx5 = new complex{ scalar{ 12 }, scalar{ 13 } };
	cx5->real = scalar{ 14 }; // Move assigment. Calls destructor on replaced scalar.
	delete cx5;

	// Composing a class on the heap with pointer members.

	cout << "=====" << endl;
	complex_ptr_members* cx6 = new complex_ptr_members{ new scalar{20}, new scalar{21} };
	delete cx6;

	cout << "=====" << endl;
	complex_ptr_members* cx7 = new complex_ptr_members{};
	delete cx7;


	cout << "=====" << endl;


	/**********
	Lvalues, rvalues, lvalue references, rvalue references.
	**********/

	// Lvalue: Anything we can apply the address operator "&" to. E.g. x, arr[3], (*ptr_x), lval_ref_a, rval_ref_b. 
	// Rvalue: Anything we can't apply the address operator to. E.g. 3, (4 + 3), cube(3), generate_and_return_point_2d().

	// Reference: A const pointer which is implicitly dereferenced by the compiler.

	// Lvalue reference: A reference which holds the address of an lvalue.
	// Rvalue reference: A reference which holds the address of an rvalue.

	// Note that we can apply "&" to lvalue refs and rvalue refs, and therefore they are lvalues.

	// Lvalue and lvalue reference examples.

	int w = 90;
	int* ptr_w = &w;
	int& lval_ref_w = w; // Address operator & is implicit for w.
	lval_ref_w = 91; // Dereference operator * is implicit for lval_ref_w.
	cout << w << endl;
	cout << lval_ref_w << endl;

	int* ptr_lval_ref_w = &lval_ref_w; // Remember, there is an implicit * for lval_ref_w. This line captures the address stored in w (not the address of w).
	// +		ptr_w	0x00000014466ff494 {3}	int *
	// +		ptr_lval_ref_w	0x00000014466ff494 {3}	int *

	int& lref_ret = return_lref(w);
	lref_ret = 92;
	cout << w << endl;

	return_lref(w) = 93;
	cout << w << endl;

	int arr[]{ 1, 2, 3 };
	int& third_int = get_indexed_location(arr, 2);
	third_int = 8;
	cout << arr[2] << endl;

	get_indexed_location(arr, 1) = 9;
	cout << arr[1] << endl;

	// Lvalue references are useful for passing large objects to a function without the overhead of creating a copy.
	// You can accomplish something similar by passing a pointer, but lvalue references have some compiler enforced safety features.
	// They are constant, and can never be NULL.

	// They are also useful for defining a [] operator for a custom class which returns a memory location that can be assigned to.

	// They make writing templates easier because a single function prototype can capture int and int&. http://www.cplusplus.com/articles/z6vU7k9E/

	// Another thing to note: If &lval_ref_w gives the address stored in lval_ref_w, how do we get the actual address of lval_ref_w?
	// We can't. Nevertheless, because we can apply the address operator & to lvalue references and rvalue references, they are considered lvalues.
	// See the discussion here:
	// https://stackoverflow.com/questions/1950779/is-there-any-way-to-find-the-address-of-a-reference

	// See the discussion here for some differences between pointers and references:
	// https://stackoverflow.com/questions/57483/what-are-the-differences-between-a-pointer-variable-and-a-reference-variable-in

	// One disadvantage of references is that they allow a function to modify an object, even if we didn't pass the object address.
	// This means you cannot tell whether an object is modified simply by reading the function call. You must declare your argument const or go look at the function definition.

	// Two good reasons to pass by lvalue reference. (Before lvalue references, these were good reasons to use pointers.)
	// 1) The function needs to modify its parameter.
	// 2) Copying the variable to the stack to pass it to the function is expensive.
	// http://www.cplusplus.com/articles/z6vU7k9E/

	// Rvalue and rvalue reference examples.

	// Rvalue references can bind to rvalues, but not lvalues.
	int&& rval_ref = 3 + 4; // Ok.
	int x = 7;
	//int&& rval_ref_x = x; // error: an rvalue reference cannot be bound to an lvalue.
	int&& rval_ref_move = std::move(x); // Ok. std::move() casts x to an rvalue.
	rval_ref_move = 8;
	cout << x << endl;

	// Why are rvalue references useful? In a single scope, they are not very useful.
	// They do extend the life of rvalues (which are temporary and normally disappear immediately),
	// but in a single scope we can accomplish the same thing with a regular variable.

	// Rvalue references are much more useful as parameters to functions.
	// To see why, first we must first know that we can trigger different overloaded functions based on whether we pass an lvalue or rvalue.
	// Here we will learn something very nuanced about C++. Function calls match function declarations based not on the types of the argument _variables_,
	// but rather based on the types of the argument _expressions_. We have not noticed this before because in most cases, the variable type and expression type are the same.
	// Todo: Is this better explained by saying that both int and int& are captured by baz(int& x)?

	cout << "=====" << endl;

	int val_p = 7;
	int& lval_ref_p = val_p;
	int&& rval_ref_p = 7;
	cout << baz(val_p) << endl;					// 8 (lvalue reference)
	cout << baz(7) << endl;						// 9 (rvalue reference)
	cout << baz(lval_ref_p) << endl;			// 8
	cout << baz(rval_ref_p) << endl;			// 8 This one might be surprising. Remember that an rvalue reference is an lvalue. (We can apply & to it.)
	cout << baz(std::move(val_p)) << endl;		// 9
	cout << baz(std::move(lval_ref_p)) << endl;	// 9

	cout << "=====" << endl;

	// Now, knowing this, how is it useful?
	// Not sure...
	// For one, it allows us to overload the copy constructor with a move constructor which accepts temporary objects which can be "pilfered".

	/**********
	When are rvalue references useful?
	**********/

	// why are rvalue references useful? (updated 2019)
	// https://stackoverflow.com/questions/5481539/what-does-t-double-ampersand-mean-in-c11?noredirect=1&lq=1
	// "The following code both invoke the move constructor for f1 and f2:"
	//foo f1((foo())); // Move a temporary into f1; temporary becomes "empty" // tested this... doesn't seem to call move constructor. 2017 optimization?
	//foo f2 = std::move(f1); // Move f1 into f2; f1 is now "empty"


	// "Smart pointers can be passed to functions in five or seven different ways"
	// https://www.internalpointers.com/post/move-smart-pointers-and-out-functions-modern-c


	// move semantics, rvalue references, C++2017
	// https://www.internalpointers.com/post/c-rvalue-references-and-move-semantics-beginners
	// "I tried your code: the move constructor never gets called!" -- thank you, thank you, thank you.
	// "The regular constructor is called instead: this is due to a trick called Return Value Optimization (RVO)."

	// https://www.internalpointers.com/post/c-rvalue-references-and-move-semantics-beginners (2018)
	// "On line (1) the literal constant 666 is an rvalue: it has no specific memory address, 
	// except for some temporary register while the program is running. It needs to be stored in a lvalue (x) to be useful."

	// "This wouldn't be possible without rvalue references and its double ampersand notation."
	// Wrong? I tried it. It works:
	std::string s1 = "foo";
	std::string s2 = "bar";
	std::string s3 = s1 + s2;
	s3 += "baz";
	cout << s3 << endl;

	// Aha! I can demonstrate move constructor by instantiating a class and passing two member classes!
	// todo...

	// https://stackoverflow.com/a/18203453/2512141 (2013)
	// "If you need one single argument why rvalue references are essential, it's unique_ptr."

	// https://www.quora.com/What-is-the-use-of-an-R-value-reference-variable-like-int
	// "Rvalue references are very rarely useful outside of function parameters.
	// I've seen rvalue reference class members being useful, 
	// but I don't think I've ever seen or needed a local rvalue-reference variable."




	/**********
	History of lvalue references, rvalue references.
	**********/

	// UPDATE! After this section was started, Herb Sutter gave an excellent overview of reference history.
	// https://herbsutter.com/2020/02/23/references-simply/
	// "Aren't local references useful because of lifetime extension?
	// We 'made it useful' as an irregular extension, but that�s brittle and now basically unnecessary as of C++17."
	// "unnecessary now that C++17 has guaranteed copy elision"

	// "But what about using a reference type as a class data member?"
	// "Don�t, see previous. People keep trying this, and we keep having to teach them not to try 
	// because it makes classes work in weird and/or unintended ways."

	// So, lvalue reference was introduced first and was very useful.
	// Later, rvalue reference was introduced and enabled move semantics, which was a big performance boost.
	// Then, (2017?) RVO removed most of the use cases where move semantics would be useful.
	// But move semantics still useful for swap(). (?)

	// https://akrzemi1.wordpress.com/2018/05/16/rvalues-redefined/
	// C++17 introduced guaranteed copy elision.
	// "C++ has completely changed the meaning of rvalue (actually, prvalue)."
	// Great...
	// In C++17:
	// X x = f();
	// is equivalent to:
	// X x(0);
	// "There are no temporaries involved. The type does not have to be movable."

	// What optimization does move semantics provide if we already have RVO?
	// https://stackoverflow.com/questions/5031778/what-optimization-does-move-semantics-provide-if-we-already-have-rvo
	// (points out that move constructor is not called due to RVO)
	// another answer points to Stroustrup's example, the swap function.

	// is C++ move constructor obsolete? (2018)
	// "Apparently, the move constructor is not used much anymore because of Return Value Optimization (RVO)."
	// https://stackoverflow.com/questions/51959379/is-c-move-constructor-obsolete






	/**********
	Initialization
	**********/

	// Types: fundamental, compound, trivial, POD, literal, ...
	// https://en.cppreference.com/w/cpp/language/types
	// https://en.cppreference.com/w/cpp/language/type

	//https://greek0.net/cpp/initialization.html
	// default, value, direct, copy, value-list, direct-list, copy-list, aggregate, reference

	int init1; // Default initialization. Primitive types are uninitialized.
	int init2{}; // Value initialization. Primitive types are set to default value. Super helpful naming here. :/ 
	// ...

	// Zero, Default, and Value initialization (as of C++11)
	// https://akrzemi1.wordpress.com/2011/06/29/brace-brace/

	// Direct-list initialization
	square_2d square_2_1{ point_2d{1, 2}, point_2d{3, 4} };

	// Copy-list initialization Generates difference disassembly than previous example.
	square_2d square_2_2 = square_2d{ point_2d{1, 2}, point_2d{3, 4} };

	// The {...} is called a braced-init-list.

	// This is direct-list-initialization vs copy-list-initialization.
	// It can affect which constructor is called for classes.
	// https://stackoverflow.com/questions/28569030/what-is-the-difference-if-any-between-x-and-x-initialization?noredirect=1&lq=1
	// https://stackoverflow.com/questions/20733360/does-the-equal-sign-make-a-difference-in-brace-initialization-eg-t-a-vs
	// direct-list vs copy-list also captured here:
	// https://en.cppreference.com/w/cpp/language/list_initialization
	// direct-list-initialization: initialization of a named variable with a braced-init-list.
	// copy-list-initialization: initialization of a named variable with a braced-init-list after an equals sign. (only non-explicit constructors may be called)
	//cout << square_2.top_left.x << endl;

	// https://stackoverflow.com/questions/18222926/why-is-list-initialization-using-curly-braces-better-than-the-alternatives
	// What is std::initializer_list constructor?
	// is example below still current in C++17?
	//struct Foo
	//{
	//	Foo() {} 
	//	Foo(std::initializer_list<Foo>) { std::cout << "initializer list" << std::endl; }
	//	Foo(const Foo&) { std::cout << "copy ctor" << std::endl; }
	//};
	//int main()
	//{
	//	Foo a;
	//	Foo b(a); // copy ctor
	//	Foo c{ a }; // copy ctor (init. list element) + initializer list!!!
	//}

	// Core guidelines recommend using {}, with a few exceptions:
	// https://github.com/isocpp/CppCoreGuidelines/blob/master/CppCoreGuidelines.md#es23-prefer-the--initializer-syntax

	// "Use ={...} if you really want an initializer_list<T>"
	//auto fib10 = {1, 1, 2, 3, 5, 8, 13, 21, 34, 55};   // fib10 is a list
	// https://github.com/isocpp/CppCoreGuidelines/blob/master/CppCoreGuidelines.md#es23-prefer-the--initializer-syntax

	// Need some term to distinguish classes which have pointer members from classes which have no pointer members.
	// POD? Trivial? IsPOD?
	// https://en.cppreference.com/w/cpp/named_req/PODType
	// https://en.cppreference.com/w/cpp/named_req/TrivialType
	// https://en.cppreference.com/w/cpp/types/is_pod
	// Hm, no, POD won't work. A class with no explicit ctors but one private var is not POD."
	// p. 49 of "A Tour of C++":
	// "We call the types that can be built out of the fundamental types, the const modifier, and the declarator operators built-in types."
	// The most common declarator operators: *, *const, &, [], ()

	// Good discussion of () vs {}.
	// https://stackoverflow.com/questions/18222926/why-is-list-initialization-using-curly-braces-better-than-the-alternatives
	// "The only situation where = is preferred over {} is when using auto keyword to get the type determined by the initializer."
	//auto z1{ 99 };   // z1 is an int
	//auto z2 = { 99 }; // z2 is std::initializer_list<int>
	//auto z3 = 99;   // z3 is an int
	// But, see disagreeing comments.
	// And see this comment:
	// "There is also the fact that using () can be parsed as a function declaration. 
	// It is confusing and inconsistent that you can say T t(x,y,z); but not T t(). And sometimes, you certain x, you can't even say T t(x);"



	/**********
	std::array
	**********/

	cout << "=====" << endl;

	// Pass, return std::array and std::vector.
	array<scalar, 2> two_scalars; // Note: just declaring the array creates and initializes the objects.
	two_scalars[0].x = 2;
	two_scalars[1].x = 5;
	print_two_scalars(two_scalars); // Expecting copies here. Yup, two copy constructors called, then two destructors.
	print_two_scalars_ref(two_scalars); // Avoids copies.

	// The only way to return std::array from a function is to know the array size at compile time.
	array<scalar, 2> two_more_scalars = return_two_scalars(); // Two constructors, two move constructors, two destructors. Was hoping RVO would happen.
	cout << two_more_scalars[0].x << endl;
	array<scalar, 2> two_more_scalars_ref = return_two_scalars_ref(); // Two constructors, two destructors, two copy constructors.
	cout << two_more_scalars_ref[0].x << endl; // Junk!!
	array<scalar, 2> two_more_scalars_ref_initzn_list{ return_two_scalars_ref() }; // Two constructors, two destructors, two copy constructors.
	cout << two_more_scalars_ref_initzn_list[0].x << endl; // Junk!!
	
	// Lesson: Don't return by reference?
	// https://stackoverflow.com/questions/752658/is-the-practice-of-returning-a-c-reference-variable-evil

	cout << "=====" << endl;


	/**********
	std::vector, and unique_ptr
	**********/

	// Functions can return vectors have length unknown at compile time.

	// Adding objects to vectors. When are copies made? When are destructors called?

	{
		vector<scalar> vec_scalars;
		vec_scalars.push_back(scalar{ 88 }); // 1-int constructor, move constructor, destructor.
	} // destructor

	{
		vector<scalar*> vec_scalars;
		vec_scalars.push_back(new scalar{ 89 }); // 1-int constructor
	} // No destructor called. Memory leak!!

	{
		vector<unique_ptr<scalar>> vec_scalars;
		vec_scalars.push_back(unique_ptr<scalar>{new scalar{ 90 }}); // 1-int constructor
	} // Destructor.

	{
		vector<scalar> vec_scalars;
		vec_scalars.emplace_back(91); // 1-int constructor. Very weird. Seems emplace_back passes args on to object constructor somehow.
	} // destructor

	{
		vector<scalar> vec_scalars;
		vec_scalars.push_back(std::move(scalar{ 92 })); // 1-int constructor, move constructor, destructor.
	} // destructor

	{
		vector<scalar> vec_scalars;
		vec_scalars.emplace_back(std::move(scalar{ 92 })); // 1-int constructor, move constructor, destructor
	} // destructor

	{
		vector<scalar> vec_scalars;
		scalar temp{ 95 }; // 1-int constructor
		vec_scalars.emplace_back(std::move(temp)); // move constructor
	} // two destructors



	cout << "=====" << endl;

	/**********
	Const
	**********/

	// const vs constexpr
	// https://www.learncpp.com/cpp-tutorial/const-constexpr-and-symbolic-constants/

	// const variables can be set at compile time or once at runtime, and cannot be changed after.
	const int int_const_compiletime{ 8 };
	int for_const = 3;
	const int int_const_runtime{ for_const };

	// constexpr variables must be set at compile time.
	//constexpr int int_constexpr{ for_const }; // compiler error: expression must have a constant value.
	constexpr int int_constexpr{ 4 };


	/**********
	Global vars
	**********/

	// Local vars: no linkage. Global vars: can have internal (single file) linkage or external (multi-file) linkage.
	// https://www.learncpp.com/cpp-tutorial/internal-linkage/

	// Static means 3 things in 3 contexts:
	// Global variables have static duration, static keyword gives global identifier internal linkage,
	// static keyword applied to local variable changes duration to static.
	// https://www.learncpp.com/cpp-tutorial/static-local-variables/

	/**********
	Enum types, Enum classes
	**********/

	// https://www.learncpp.com/cpp-tutorial/45-enumerated-types/
	// https://www.learncpp.com/cpp-tutorial/4-5a-enum-classes/




	/**********
	Misc.
	**********/

	// std::array, std::vector
	// https://www.learncpp.com/cpp-tutorial/6-15-an-introduction-to-stdarray/
	// https://www.learncpp.com/cpp-tutorial/6-16-an-introduction-to-stdvector/

	// RAII

	// SBRM "Scope-Bound Resource Management", aka RAII "Resource Acquisition is Initialization".
	// https://embeddedartistry.com/blog/2017/07/17/migrating-from-c-to-c-take-advantage-of-raii-sbrm/

	// Optimization: passing const refs.

	// Composing containers.

	// Composing classes. (subclasses live on stack or on heap?)

	// Using unique_ptr to improve memory safety for pointers.
	// Using unique_ptr to improve memory safety for arrays.

	// Example of RVO?
	vector<int> rvo = return_fours(8);
	cout << rvo.size() << endl;

	// Example of using a returned lvalue reference to populate a custom data structure.

	// For classes, setting default vals for a member is called class member initializer.
	// http://www.informit.com/articles/article.aspx?p=1852519

	// C++17 linked list with smart pointers.
	// https://solarianprogrammer.com/2019/02/22/cpp-17-implementing-singly-linked-list-smart-pointers/

	// An entire article just on the declarations for an STL-compatible container...
	// https://medium.com/@vgasparyan1995/how-to-write-an-stl-compatible-container-fc5b994462c6

	// Storage class specifiers static, extern, mutable.
	// https://www.learncpp.com/cpp-tutorial/internal-linkage/
	// On second thought, I probably want to avoid global variables, so extern would not be useful?
	// Maybe static and mutable are still useful...

	// Implementing factory method in C++
	// https://stackoverflow.com/questions/5120768/how-to-implement-the-factory-method-pattern-in-c-correctly?noredirect=1&lq=1

	// https://web.mst.edu/~nmjxv3/articles/move-gotchas.html
	// C++ Move Semantics Gotchas (not sure about date/currency)
	// Calls copy constructor.
	//friend foo operator+(foo lhs, const foo& rhs)
	//{
	//	cerr << "Do +\n";
	//	return lhs += rhs;
	//	
	//}

	// Calls move constructor.
	//friend foo operator+(foo lhs, const foo& rhs)
	//{
	//	cerr << "Do +\n";
	//	lhs += rhs;
	//	return lhs;
	//	
	//}

	// With unique_ptr, how do we manually tell program we are done with pointed-to object?
	// reset()? http://www.cplusplus.com/reference/memory/unique_ptr/release/

	// I also very much like this guy's (Klaus Iglberger) definition of lvalue:
	// https://youtu.be/St0MNEU5b0o?t=654
	// Anything that I can pick a name for.
	// rvalues were introduced in C++11 to solve the following problem:
	// v2 = v1; // causes a copy, which is what we want.
	// v2 = create_vector(); // would have caused a copy, which is not what we want.
	// Aha!
	// v2 = std::move(v1); // v1 is an lvalue, move() converts it to an rvalue, or more specifically, an xvalue (!!)
	// 20:30 - if using fundamental types, classes, and unique ptr, we can use default move constructor, default move assignment.
	// If using raw pointers, we must define the move constructor, move assignment ourselves.

	//for(size_t i = 0UL; i < N; i++){
	//  Widtget w{1, s, nullptr};
	//  v.push_back(std::move(w)); // canonical way to add objects to a vector?
	//}

	// nice: default move constructor is noexcept.

	// if you move a unique_ptr<>, it automatically "nulls" the source pointer?

	// canonical forms of move constructor, move assignment. 
	// both with and without smart pointers. (versions with smart pointers are identical to defaults.

	//-----

	//class Widget{
	//private:
	//  int i{0};
	//  std::string s{};
	//  int* pi{nullptr};
	//
	//public:
	//  ...
	//  Move constructor
	//  Widget(Widget&& w) noexcept
	//    :i(std::move(w.i))
	//    ,s(std::move(w.s))
	//    ,pi(std::move(w.pi))
	//  {
	//    w.pi = nullptr;
	//  }
	//  ...
	//};

	//class Widget{
	//private:
	//  int i{0};
	//  std::string s{};
	//  unique_ptr<int> pi{};
	//
	//public:
	//  ...
	//  Move constructor
	//  Widget(Widget&& w) noexcept
	//    :i(std::move(w.i))
	//    ,s(std::move(w.s))
	//    ,pi(std::move(w.pi))
	//  {
	//  }
	//  ...
	//};

	//class Widget{
	//private:
	//  int i{0};
	//  std::string s{};
	//  unique_ptr<int> pi{};
	//
	//public:
	//  ...
	//  // Move constructor
	//  Widget(Widget&& w) = default;
	//  ...
	//};

	//-----

	//class Widget{
	//private:
	//  int i{0};
	//  std::string s{};
	//  int* pi{nullptr};
	//
	//public:
	//  ...
	//  // Move assignment operator
	//  Widget& operator=(Widget&& w) noexcept
	//  {
	//    delete pi;
	//    i = std::move(w.i);
	//    s = std::move(w.s);
	//    pi = std::move(w.pi);
	//    w.pi = nullptr;
	//
	//    return *this;
	//  }
	//  ...
	//};

	//class Widget{
	//private:
	//  int i{0};
	//  std::string s{};
	//  unique_ptr<int> pi{};
	//
	//public:
	//  ...
	//  // Move assignment operator
	//  Widget& operator=(Widget&& w) noexcept
	//  {
	//    i = std::move(w.i);
	//    s = std::move(w.s);
	//    pi = std::move(w.pi);
	//
	//    return *this;
	//  }
	//  ...
	//};

	//class Widget{
	//private:
	//  int i{0};
	//  std::string s{};
	//  unique_ptr<int> pi{};
	//
	//public:
	//  ...
	//  // Move assignment operator
	//  Widget& operator=(Widget&& w) = default; // Note: default is noexcept.
	//  ...
	//};

	//-----

	// Wait, what? template types for a class are different than template types for a function?
	// https://youtu.be/pIzaZbKUw2s?t=2085
	// https://stackoverflow.com/questions/14040329/difference-between-class-template-and-function-template
	// https://northstar-www.dartmouth.edu/doc/ibmcxx/en_US/doc/language/ref/rncldiff.htm
	// Hm, no, I may have misread. It seems both need to be "stamped out"?
	// See "function template declaration", "function template definition".
	// https://youtu.be/LMP_sxOaz6g?t=339
	// No, this is wrong. I'm still confused...
	// There is a "template parameter list" and "function parameter list".
	// constexpr, consteval both switch runtime computations into compile-time computations?
	// @9:12, "C++ compilers automatically generate function definitions from templates as needed."
	// "The act of generating a function definition from a template is called 'template instantiation'".
	// Hm, @19:00 he starts discussing class templates, so he may cover the difference between the two.
	// What? "The compiler automatically instantiates class definitions as needed." Maybe in my case the class template is compiled in a separate unit?
	// These do the same thing:
	// typedef rational<int> irat;
	// using irat = rational<int>;
	// https://youtu.be/LMP_sxOaz6g?t=2221
	// Hm, but he does say that template declarations, including the definitions, go in headers.
	// He says that if multiple places include the header, we would usually get multiple definition linker errors,
	// but that modern linkers are smart enough to know to discard the duplicates.


	// virtual vs pure virtual functions.
	// https://stackoverflow.com/questions/1306778/virtual-pure-virtual-explained

	// "Return Value Optimization is a common form of copy elision."
	// https://stackoverflow.com/questions/12953127/what-are-copy-elision-and-return-value-optimization

	// CppCon, Back to Basics playlist
	// https://www.youtube.com/playlist?list=PL5qoVlA-tv09ykIIPHP9N6vgJaFPnYWCa

	// Static vs dynamic polymorphism
	// http://www.tutorialdost.com/Cpp-Programming-Tutorial/56-CPP-Polymorphism-Static-Dynamic.aspx

	// Bjarne Stroustrup on where to put template implementation:
	// https://www.bogotobogo.com/cplusplus/template_declaration_definition_header_implementation_file.php
	// "[T]emplate writers tend to place template definition in header files.
	// ... [U]ntil improved implementations are widely availble, we recommend that you do so for your own templates:
	// place the definition of any template that is to be used in more than one translation unit in a header file.

	// So then, the question arises: What belongs in .cpp files? Is it only executables?

	// #pragma once seems to be supported in all compilers now, but it might break if you have two copies of the same file in your build.
	// https://stackoverflow.com/a/1946730/2512141
	// The alternative is to use header guards.

	// more discussions on splitting up template and implementation.
	// https://raymii.org/s/snippets/Cpp_template_definitions_in_a_cpp_file_instead_of_header.html
	// http://blog.ethanlim.net/2014/07/separate-c-template-headers-h-and.html
	// https://softwareengineering.stackexchange.com/questions/373916/c-preferred-method-of-dealing-with-implementation-for-large-templates (mentions .tpp approach)
	// https://stackoverflow.com/questions/115703/storing-c-template-function-definitions-in-a-cpp-file?rq=1
	// https://stackoverflow.com/questions/11677223/c-can-i-use-a-template-function-that-is-not-implemented-in-a-header-file?noredirect=1&lq=1
	// No good solutions here, but some hope that headers will be deprecated/made less important in C++20.
	// https://hackaday.com/2019/07/30/c20-is-feature-complete-heres-what-changes-are-coming/
	// As of 2020-04-26, modules only supported in gcc, clang.
	// Coming to VS2019? https://docs.microsoft.com/en-us/cpp/cpp/modules-cpp?view=vs-2019

}