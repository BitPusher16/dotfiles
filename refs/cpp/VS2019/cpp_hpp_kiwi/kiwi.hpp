#pragma once

#include <iostream>
#include <bitset>

// Still on my quest to capture C++ essentials in a single file.
// Have decided to use a single .cpp and single .hpp, since current best practice is to implement template classes in headers.

// Two sections: Memory-safe and memory-unsafe.
// (Memory unsafe will include data structures with cycles, raw pointers.)
// Edit: Is it possible to create loops using unique pointers? Yes, afraid so... So even with unique pointers, memory safety is not guaranteed.
// But what if we only assigned pointers at object construction? Then, there would be no way to create a loop...
// But there would also be no way to modify the data structure.
// Wait! What if we did not allow the function which moved a unique pointer? Well, then no loops are possible, and building out data structures is possible (e.g., growing a tree),
// but "re-wiring" data structures is not possible (e.g., balancing a tree)...
// Wait, no, there's something I've been missing. unique_ptr destroys the child object when leaving scope, but also if it has another child assigned to it...
// With this guarantee, I don't think there is a way to create loops...

// Do not cover every way to accomplish a task. Include only the most modern, idiomatic version.
// Edit: This means only cover struct or class, not both.
// Edit: Is there ever a reason to use inheritance? Why not just use interfaces?

// Design idea: By default, when writing a class, keep all functions separate from the class. (Except xtors.)
// Then, if there is some function that absolutely must be hidden from the consumer, move it inside the class.
// (But then what is the use case for class public functions?)

// Observation: In earlier versions of C++, by default, the var could not be changed when passed to a function.
// You had to use & to pass the var address.
// Now, by default, the var can be changed if the function takes an lref, and to keep the var unchanged you must pass it as const. (?)

// Lots of disagreement over whether general-use functions should go in a namespace or a class.
// https://softwareengineering.stackexchange.com/questions/134448/where-should-i-put-functions-that-are-not-related-to-a-class
// https://softwareengineering.stackexchange.com/questions/134540/are-utility-classes-with-nothing-but-static-members-an-anti-pattern-in-c?noredirect=1&lq=1
// However, one user points out that namespaces can't be templated.
// For me, that is a strong argument for using classes.

// three links to quickly summarize the C++ header template fiasco.
// https://stackoverflow.com/questions/4955159/is-is-a-good-practice-to-put-the-definition-of-c-classes-into-the-header-file
// https://stackoverflow.com/questions/495021/why-can-templates-only-be-implemented-in-the-header-file?rq=1
// https://isocpp.org/wiki/faq/templates#templates-defn-vs-decl
// can this be solved with PIMPL idiom? No.
// Look into modules (C++20)
// https://docs.microsoft.com/en-us/cpp/cpp/modules-cpp?view=vs-2019

// Try to have only 2 structs in this project:
// A POD class,
// and a class with int, raw pointer, smart pointer, with all xtors defined (having unique per-object ids, and console notifications).
// What about inheritance/interfaces?

//interface: vessel
//abstract class: dining set
//class: plate
//class: cup
//class: saucer

namespace kiwi
{

void functify()
{

	// We can place "using" statements in global, namespace, function, or class scope.
	// Putting "using" in global modifies all consumers that include the header.
	// Putting "using" in namespace modifies all consumers that use our namespace.
	// Putting "using" in function or class limits using to that func/class, which is better.
	using std::cout;
	using std::endl;
	using std::bitset;

	// https://en.cppreference.com/w/cpp/types/integer
	using i8 = int8_t;
	using ui8 = uint8_t;
	using i32 = int32_t;
	using ui32 = uint32_t;
	using f32 = float_t;
	using f64 = double_t;

	i32 foo{ 200 };
	bitset<8> bs{ 0b0010 };

	cout << "beg functify()" << endl;

	// Topics to cover: 
	// templates
	// inheritance
	// interfaces
	// smart pointers
	// raw pointers
	// xtors
	// standard library
	// composing classes
	// composing standard library data structures
	// passing/returning/modifying classes and data structures
	// 

	cout << "end functify()" << endl;
}

}