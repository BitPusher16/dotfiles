
#include <iostream>
#include <vector>

using std::cout;
using std::endl;
using std::vector;

void scratch()
{
	// this space is for experimentation.
	// useful snippets should eventually move to better-categorized files.

	// vector of vectors
	int rows = 3;
	int cols = 4;

	// these 2 initializations appear to do the same thing.
	// is there a performance difference?
	vector<vector<int>> matrix(rows, vector<int>(cols));
	//vector<vector<int>> matrix = vector<vector<int>>(rows, vector<int>(cols));

	matrix[1][2] = 55;
	matrix[1][3] = 56;
	matrix[2][0] = 33;
	for (int i = 0; i < matrix.size(); i++)
	{
		for (int j = 0; j < matrix[i].size(); j++)
		{
			cout << matrix[i][j] << ",";
		}
		cout << endl;
	}

	// todo: hashmap of vectors.



	cout << "done" << endl;
}