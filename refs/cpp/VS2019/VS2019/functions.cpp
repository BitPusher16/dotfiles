


// there are a number of nuances to be alert for when calling functions.
// - what happens when large objects are passed to a function? are they copied? can this be done more efficiently?
// - what happens when the function returns large objects? are they copied? can this be done more efficiently?
// - what happens if the function allocates memory with malloc() or new?


// https://stackoverflow.com/questions/15704565/efficient-way-to-return-a-stdvector-in-c
// returning by value vs returning by pointer.
// (preferred method in C++11 is return by value.)
//std::vector<T> f()
//{
//	std::vector result;
//	/* Insert elements into result */
//	return result;
//}


// it is not legal to return an lvalue reference to a local var,
// so those functions can only return lrefs to static vars?

// this says to return an object by value instead of reference?
// https://stackoverflow.com/questions/1116641/is-returning-by-rvalue-reference-more-efficient

// update: at least 3 ways to return a reference from a function:
// 1) return a static variable
	// https://www.tutorialspoint.com/cplusplus/returning_values_by_reference.htm
// 2) return a global variable
	// https://www.programiz.com/cpp-programming/return-reference
// 3) return a reference which was passed in. (probably the most useful case?)
	// https://www.learncpp.com/cpp-tutorial/74a-returning-values-by-value-reference-and-address/

//Foo& return_obj_ref()
//{
//	Foo f = new Foo();
//}
	// this doesn't work because "Foo f" declares an object (which is allocated on the stack), not a pointer to an object.
	// this is conflicting with my Java muscle memory, I think.
	// e.g. (Java) Box box = new Box();

// good reference on returning references.
// "Don't Help the Compiler", by Stephan T. Lavavej
// https://channel9.msdn.com/Events/GoingNative/2013/Don-t-Help-the-Compiler
// https://view.officeapps.live.com/op/view.aspx?src=http%3a%2f%2fvideo.ch9.ms%2fsessions%2fgonat%2f2013%2fSTLGN13Compiler.pptx

// slide 22
// avoid new and delete, prefer make_shared and make_unique.
// avoid new[] and delete[], use vector and string.
// avoid manual resource management.
// when manual resource management cannot be avoided, write classes to handle.

// see slide 28
// string&& meow() { string s; stuff; return s; } // WRONG
// string& meow() { string s; stuff; return s; } // WRONG
// string meow() { string s; stuff; return s; } // RIGHT

// slide 25
// string meow() { string ret; stuff; return move(ret); } // BAD
// string meow() { string ret; stuff; return ret; } // BETTER (enabled NRVO)

// slide 29
// don't return const value.
// don't move() when returning local x by value x
// don't return rvalue reference (use cases extremely rare)


#include <iostream>
#include "tea_set.h"

using std::cout;
using std::endl;

void pass_by_value(int var) { var += 2; }
void pass_by_ptr(int* var_ptr) { *var_ptr += 2; }
void pass_by_lval_ref(int& var_lval_ref) { var_lval_ref += 2; }
void pass_by_rval_ref(int&& var_rval_ref) { var_rval_ref += 2; }

int return_val() { return 3; }
int* return_ptr()
{
	//int tmp = 4;
	//return &tmp;

	// is this valid?
	// it does compile and run.
	// how long does tmp stay in scope?

	// hm, seems what I have written above might have undefined behavior.
	// https://stackoverflow.com/questions/20063617/why-is-it-bad-practice-to-return-a-pointer-to-a-local-variable-or-parameter

	int* int_ptr = new int;
	*int_ptr = 4;
	return int_ptr;
		// CAUTION: this might not be idiomatic because
		// the caller is responsible for calling delete on the returned pointer.
}
int& return_lval_ref(int& int_lval_ref)
{
	return int_lval_ref;
		// this is pretty weird.
		// the variable returned by this statement can be assigned to in the calling program. (?!)
		// I thought references could only be assigned to at instantiation?
		// I suppose no references are being declared here...
		// makes a little more sense if you think of a reference as "a pointer automatically dereferenced by the compiler".
		// in that case, we are returning an address, and assigning to the dereferenced address.
		// no idea how this would work if we returned a ref to an object...
		// after thinking about it, I guess it would be just like returning a pointer to an object...
}
int&& return_rval_ref(int& int_lval_ref)
{
	return std::move(int_lval_ref);
		// this doesn't do much that is useful.
		// more useful when defining class operators?
}



// pass-by-value functions make a copy of all arguments.
// this may be expensive or undesirable for an argument that is a struct or class.
// to avoid this, pass a reference.

// to keep the class from being modified when passing by reference, pass a constant reference.
// NOTE: this will not prevent the function from dereferencing pointers within the class and changing their addressed values.

// wait, Herb Sutter says we no longer need to pass const?
// https://stackoverflow.com/questions/10231349/are-the-days-of-passing-const-stdstring-as-a-parameter-over?rq=1

void try_to_modify_const(const tea_set& ref_ts)
{
	//ref_ts.id = 4;
		// not allowed.
	ref_ts.ptr_c->volume = 3;
		// allowed!
}

// this function returns a pointer to an array, but it always returns the same address.
// not sure when this would be useful...
int* return_static_ptr()
{
	static int arry[3];
	return arry;
}

// function pointer
int times_two(int a) { return 2 * a; }

// function with default params
int times_three(int a = 4) { return 3 * a; }

void functions()
{

	// passing value, ptr, lvalue ref, rvalue ref

	int var = 3;
	pass_by_value(var);
	cout << "after pass by value: " << var << endl;

	int* var_ptr = &var;
	pass_by_ptr(var_ptr);
	cout << "after pass by ptr: " << var << endl;

	int& var_lval_ref = var;
	pass_by_lval_ref(var_lval_ref);
	cout << "after pass by lval ref: " << var << endl;

	int x_lval_ref = 7;
	pass_by_rval_ref(std::move(x_lval_ref));
	cout << "after pass by rval ref: " << x_lval_ref << endl;


	// return value, ptr, lvalue ref, rvalue ref

	int y = return_val();
	cout << y << endl;

	int* y_ptr = return_ptr();
	cout << *y_ptr << endl;
	delete y_ptr;

	int p = 24;
	return_lval_ref(p) = 25;
	cout << p << endl;

	//return_rval_ref(p) = 30;
		// "expression must be a modifiable lvalue"
	//int r = return_rval_ref(p);
		// gets dereferenced value 25, doesn't get a ref to p.
	int&& r = return_rval_ref(p);
	r = 30;
	cout << p << endl;

	// (another way to use returned rvalue ref)
	int u = return_rval_ref(p) + 2;
	cout << u << endl;


	// passing a constant ref and modifying a member
	tea_set ts_a;
	try_to_modify_const(ts_a);
	cout << ts_a.ptr_c->volume << endl;

	// function that returns static array
	// when is this useful?
	int* arry_a = return_static_ptr();
	int* arry_b = return_static_ptr();
	cout << arry_a << "," << arry_b << endl;

	// function pointers
	int (*fnc_ptr)(int) = times_two;
	cout << (*fnc_ptr)(9) << endl;
	cout << fnc_ptr(9) << endl;

	// functions with default params.
	int ab = times_three();
	cout << ab << endl;

	// lambda functions
	// https://stackoverflow.com/questions/7627098/what-is-a-lambda-expression-in-c11
	

}