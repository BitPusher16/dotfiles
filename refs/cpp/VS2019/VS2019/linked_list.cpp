
#include <iostream>
#include "linked_list.h"

using std::cout;
using std::endl;

void linked_list()
{

	LinkedList<int> list_a;
	cout << list_a << endl;
	list_a.add(1);
	list_a.add(3);
	list_a.add(5);
	list_a.add(7);
	list_a.add(9);
	list_a.remove(1);

	LinkedList<int>::iterator iter = list_a.begin();
	while (iter != list_a.end()) { cout << *iter << ","; iter++; }
	cout << endl;

	for (auto x : list_a) { cout << x << ","; }
	cout << endl;

	cout << list_a.get(2) << endl;
	cout << list_a << endl;

	cout << "todo" << endl;
}