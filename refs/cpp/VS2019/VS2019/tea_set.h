

// the classes here are designed to help show how special member functions work.
// each class is given a unique id, which is not transferred on copy or move.
// this is not how copy and move are usually implemented.

// cup_id, spoon_id, and tea_set_id are all initialized to 0 in tea_set.cpp.
// this is required because if this .h is included in multiple files,
// the compiler will complain about multiple declarations.

// note the str() functions are all const.
// const placed after param list means the function cannot modify the object.
// we must do this because in some cases we will need to call str()
// when we've been handed a const reference to the object.

#pragma once

#include <iostream>
#include <string>

using std::cout;
using std::endl;
using std::ostream;
using std::string;
using std::to_string;

class cup { 
public: 
	static int cup_id;

	int id;
	int volume; 
	cup(int v) :volume(v) { id = cup_id++; }
	string str() const { 
		return "cup_" + to_string(id) + "(" + to_string(volume) + ")"; 
	}
};

class spoon { 
public: 
	static int spoon_id;

	int id;
	int length; 
	spoon(int l) :length(l) { id = spoon_id++; }
	string str() const { 
		return "spoon_" + to_string(id) + "(" + to_string(length) + ")"; 
	}
};

class tea_set
{
public:
	static int tea_set_id;

	int id;
	cup* ptr_c;
	spoon* ptr_s;
		// more idiomatic to have these as value classes rather than pointers or references?
		// not sure. leaving them this way for now bc i know how to handle pointers.
		// actually, probably more idiomatic to leave these as pointers than have them as part of stack.
		// if they're on the function stack, every time they're reassigned they'll have to be overwritten by copying... probably?
		// and i don't think i want to use references. bc then i can't reassign them?

		// ah, interesting. apparently this design choice has a name.
		// it is the difference between aggregation and composition.
		// also called dependency injection vs constructor injection?
		// https://stackoverflow.com/questions/12387239/reference-member-variables-as-class-members
		// https://stackoverflow.com/questions/39267388/best-practices-for-dependency-injection-via-constructor

		// this post recommends against reference members, but a comment says reference members are good?:
		// https://stackoverflow.com/questions/892133/should-i-prefer-pointers-or-references-in-member-data
		// another higher-voted answer recommends using reference to couple the object lifespans.


	string str() const { 
		return "tea_set_" + to_string(id) 
			+ "(" + (ptr_c == nullptr ? "_" : ptr_c->str()) 
			+ "," + (ptr_s == nullptr ? "_" : ptr_s->str()) + ")"; 
	}

	// parameterized constructor
	// (remember, we can also create a default constructor that is parameterized
	// by settings param defaults, but we choose not to do that here.)
	tea_set(int c, int s)
	{
		id = tea_set_id++;
		ptr_c = new cup(c);
		ptr_s = new spoon(s);
		cout << "parameterized constructor " << str() << endl;
	}

	// default constructor
	tea_set()
	{
		id = -9999;
		ptr_c = new cup(2);
		ptr_s = new spoon(1);
		cout << "default constructor " << str() << endl;
	}

	// destructor
	~tea_set()
	{
		delete ptr_c;
		delete ptr_s;
		ptr_c = nullptr;
		ptr_s = nullptr;
			// would be nice if delete would set nullptr automatically.
			// hm, Stroustrup seems to address here:
			// https://stackoverflow.com/questions/704466/why-doesnt-delete-set-the-pointer-to-null
			// suggests using your own inline "destroy" function?
		cout << "destructor " << str() << endl;
	}

	// copy constructor
	tea_set(const tea_set& other)
	{
		id = tea_set_id++;
		ptr_c = new cup(other.ptr_c->volume);
		ptr_s = new spoon(other.ptr_s->length);
		cout << "copy constructor from:" << other.str() << " to:" << str() << endl;
	}

	// copy assignment
	tea_set& operator= (const tea_set& other)
	{
		delete ptr_c;
		delete ptr_s;
		ptr_c = new cup(other.ptr_c->volume);
		ptr_s = new spoon(other.ptr_s->length);
		cout << "copy assignment from:" << other.str() << " to:" << str() << endl;

		return *this;
	}

	// move constructor
	tea_set(tea_set&& other) noexcept
	{
		id = char(tea_set_id++);
		ptr_c = other.ptr_c;
		ptr_s = other.ptr_s;
		other.ptr_c = nullptr;
		other.ptr_s = nullptr;
		cout << "move constructor from:" << other.str() << " to:" << str() << endl;
	}

	// move assignment
	tea_set& operator= (tea_set&& other) noexcept
	{
		delete ptr_c;
		delete ptr_s;
		ptr_c = other.ptr_c;
		ptr_s = other.ptr_s;
		other.ptr_c = nullptr;
		other.ptr_s = nullptr;
		cout << "move assignment from:" << other.str() << " to:" << str() << endl;

		return *this;
	}


};