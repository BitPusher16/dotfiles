

#include <iostream>
#include <string>

using std::cout;
using std::endl;
using std::string;


class foo_exception
{
public:
	string error;
	foo_exception(string err) :error{ err } {};
};

void throw_exception(int e)
{
	switch (e)
	{
		case 1 : 
			throw - 1;
		case 2:
			throw double(-1.0);
		case 3:
			throw string("whoops");
		case 4:
			throw "uh-oh";
		case 5:
			throw float(-1.0);
		case 6:
			throw foo_exception("bar");
				// what does the catch statement get? pointer? lvalue reference? rvalue reference?
		default:
			cout << "behaving..." << endl;
	}
}

void exceptions()
{
	for (int i = 0; i < 8; i++)
	{
		try { throw_exception(i); }
		catch (int n)
		{
			cout << "caught int " << n << endl;
		}
		catch (double d)
		{
			cout << "caught double " << d << endl;
		}
		catch (string s)
		{
			cout << "caught string " << s << endl;
		}
		catch (const char* c)
		{
			cout << "caught c-style string " << string(c) << endl;
		}
		catch (foo_exception f)
		{
			cout << "caught foo exception " << f.error << endl;
		}
		catch (...)
		{
			cout << "caught unknown exception type." << endl;
		}
	}
	cout << "bye" << endl;
}