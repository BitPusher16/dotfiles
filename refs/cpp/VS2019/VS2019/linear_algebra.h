#pragma once


class matrix
{
private:
	int m; // num rows
	int n; // num cols
	double* A;

public:

	matrix(int m, int n);
	~matrix();

	int numRows() { return m; }
	int numCols() { return n; }

	static matrix mat_mul(matrix A, matrix B);
};

