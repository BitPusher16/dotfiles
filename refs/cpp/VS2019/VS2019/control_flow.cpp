
#include <iostream>
#include <string>
#include <vector>

using std::cout;
using std::endl;
using std::vector;

class point_2d {public: int x; int y; point_2d(int a, int b) :x{ a }, y{ b }{}};

void control_flow()
{
	// conditionals (if, else)
	// boolean comparisons?

	if (true) { cout << "true" << endl; }
	else { cout << "false" << endl; }

	if (1) { cout << "true" << endl; }
	else { cout << "false" << endl; }

	if (0) { cout << "true" << endl; }
	else { cout << "false" << endl; }

	if (-20) { cout << "true" << endl; }
	else { cout << "false" << endl; }

	// loops
	// for, while, do while

	// break, continue

	// iteration

	// strings, arrays, containers, and any type that supports begin() end() 
	// can be iterated over.

	// range-based for loop
	int arry[] = { 1, 2, 3, 4 };
	for (auto x : arry) { cout << x << ","; }
	cout << endl;

	// range-based for loop over vector
	vector<point_2d> points{ point_2d(1,2), point_2d(3,4), point_2d(5,6) };
	for (auto p : points) { cout << p.x << ","; }
	cout << endl;

	// switch
	for (auto x : arry)
	{
		switch (x)
		{
		case 1:
			cout << "one,";
			break;
		case 2:
			cout << "two,";
			break;
		default:
			cout << "other_num,";
		}
	}

	// goto
	for (auto x : arry)
	{
		if (x == 3)
		{
			cout << endl;
			goto quit;
		}
		else
		{
			cout << x << ",";
		}
	}

quit:
	cout << "leaving control_flow()" << endl;
}