

// char array vs char pointer.
// https://www.geeksforgeeks.org/whats-difference-between-char-s-and-char-s-in-c/

// char a[10] = "foo";
// 1) a is an array
// 2) sizeof(z) = 10 bytes
// 3) a and &a are the same
// 4) a is stored in stack, foo is stored in stack
// 5) a = "bar"; invalid (bc a is a const ptr?)
// 6) a++ invalid
// 7) a[0] = 'b'; valid

// char *p = "foo";
// 1) p is pointer variable
// 2) sizeof(p) = 4 bytes
// 3) p and &p not the same
// 4) p stored in stack, foo stored in code memory section
// 5) p = "bar"; valid
// 6) p++ valid
// 7) p[0] = 'b' invalid (bc code section is read-only?)



#include <iostream>
#include <stdlib.h> // srand, rand
#include <time.h> // time
using std::cout;
using std::endl;

void arrays()
{

	// different ways to initialize
	int array_1[2];
	array_1[0] = 55;
	array_1[1] = 66;
	cout << array_1[1] << endl;

	int array_2[2]{ 88, 99 };
	cout << array_2[1] << endl;

	int array_3[]{ 9, 8 };
	cout << array_3[1] << endl;

	// array vars are fixed, pointer vars can change
	int arry10[]{ 50, 51 };
	int arry11[]{ 60, 61 };
	int* i_ptr = arry10;
	cout << arry10[0] << " " << arry11[0] << " " << i_ptr[0] << endl;
	i_ptr = arry11;
	cout << arry10[0] << " " << arry11[0] << " " << i_ptr[0] << endl;

	// static array allocation
	int arry3a[]{ 18, 19, 20 };
	cout << "size of arry3a = " << sizeof(arry3a) << endl; // 12 (3 ints * 4 bytes per int)

	// static array allocation must use array var?
	//int* tmp{ 18, 20 };
		// too many initializer values
	//int* tmp{ 18 };
		// a value of type "int" cannot be used to initialize an entity of type "int *"

	// dynamic alloc with malloc
	int* arry4a = (int*)malloc(sizeof(int) * 3); // returns NULL on failure
	if (arry4a != NULL)
	{
		arry4a[0] = 15;
		arry4a[1] = 16;
		arry4a[2] = 17;
		cout << arry4a[1] << endl;
	}
	cout << "size of arry4a = " << sizeof(arry4a) << endl; // 8 (8 bytes per pointer on 64-bit arch)
	free(arry4a);

	// dynamic alloc with malloc must use pointer var?
	//int foo[] = (int*)malloc(sizeof(int) * 4);
		// initialization with "{...}" expected for aggregate object

	// dynamic alloc with new
	int* arry4b = new int[3]; // throws exception on failure
	arry4b[0] = 21;
	arry4b[1] = 22;
	arry4b[2] = 23;
	cout << arry4b[1] << endl;
	cout << "size of arry4b = " << sizeof(arry4b) << endl; // 8 (8 bytes per pointer on 64-bit arch)
	delete[] arry4b;

	// dynamic array allocation with malloc must use pointer var?
	//int tmp[] = new int[4];
		// initialization with "{...}" expected for aggregate object

	// dynamic array allocation with new (random size)
	// http://www.fredosaurus.com/notes-cpp/newdelete/50dynamalloc.html

	srand(time(NULL));
	int iRand = rand() % 2 + 2;
	int* arry3b = new int[iRand];
	for (int i = 0; i < iRand; i++) { arry3b[i] = rand(); }
	cout << arry3b[1] << endl;
	cout << "size of arry3b = " << sizeof(arry3b) << endl; // 8 (8 bytes per pointer on 64-bit arch)
	delete[] arry3b;


	// arrays of structs/objects

	// passing arrays

	// returning arrays

	// RAII arrays?

	// arrays as members of classes?

	// multidimensional arrays
	// https://isocpp.org/wiki/faq/freestore-mgmt#multidim-arrays

}