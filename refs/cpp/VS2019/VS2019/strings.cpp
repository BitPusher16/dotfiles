
#include <iostream>
#include <string>

using std::string;
using std::cout;
using std::endl;

void strings()
{
	// newlines
	cout << "hello\n";
	cout << "world" << endl;

	// concatenation
	string s1 = "hello";
	string s2 = "friend";
	string s3 = s1 + " " + s2;
	cout << s3 << endl;

	// indexing
	cout << s1[4] << endl;
	for (int i = 0; i < s1.size(); i++) { cout << s1[i] << ' '; }
	cout << endl;

	// char array to string.
	char char_array[]{ 'h', 'i', '\0' };
	cout << string(char_array) << endl;

	// string to char array, fixed array size
	char char_array_2[20];

	//strcpy(char_array_2, s3.c_str()); // error: unsafe
	//strncpy(char_array_2, s3.c_str(), s3.size()); // error: unsafe
	strcpy_s(char_array_2, sizeof(char_array_2), s3.c_str()); 
		// null terminates after copy. if string doesn't fit in array, invokes invalid param handler.
		// if string fits but no space for null terminate, invokes invalide param handler.
	cout << string(char_array_2) << endl;


	// string to char array, dynamic array size.
	char* char_array_3 = new char[s3.size() + 1];
	strcpy_s(char_array_3, sizeof(char) * (s3.size() + 1), s3.c_str());
	cout << string(char_array_3) << endl;

	// concat literals, variables with string
	string concat = "(" + std::to_string(3) + ")";
	cout << concat << endl;


}