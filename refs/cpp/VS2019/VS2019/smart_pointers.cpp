



// RAII, raw pointer, smart pointers, unique pointers, shared pointer, auto pointer, weak pointer, scoped pointer?

// scoped_ptr is from boost
// https://www.fluentcpp.com/2017/08/25/knowing-your-smart-pointers/

// auto_ptr dangerous, deprecated in C++11
// https://stackoverflow.com/a/106614/2512141

// traditional pointer is also called raw pointer.
// https://www.internalpointers.com/post/beginner-s-look-smart-pointers-modern-c (sep 2018)

// do smart pointers call dtors?

// some of the problems with raw pointers:
// https://www.oreilly.com/library/view/effective-modern-c/9781491908419/ch04.html

// a little tricky to make a unique_ptr to an array.
// better to use make_unique.
// https://www.internalpointers.com/post/beginner-s-look-smart-pointers-modern-c
// https://stackoverflow.com/questions/21377360/proper-way-to-create-unique-ptr-that-holds-an-allocated-array

// oh dear, the weak_ptr rabit hole goes very deep indeed...
// https://dev.to/fenbf/how-a-weakptr-might-prevent-full-memory-cleanup-of-managed-object-i0i

// intro to smart pointers and appropriate use cases
// https://embeddedartistry.com/blog/2017/01/04/c-smart-pointers/
// https://embeddedartistry.com/blog/2017/07/12/ditch-your-c-style-pointers-for-smart-pointers/

// smart pointers and aligned malloc/free
// https://embeddedartistry.com/blog/2017/03/01/c-smart-pointers-with-aligned-malloc-free/

// make_unique is a C++14 feature.
// https://embeddedartistry.com/blog/2017/01/04/c-smart-pointers/

// if you return a unique_ptr, the compiler calls move() for you.
// https://stackoverflow.com/questions/4316727/returning-unique-ptr-from-functions

// assignments between stack objects perform a copy from right side to left.
// https://visualstudiomagazine.com/articles/2012/08/13/c-plus-copy-semantics.aspx

// std::unique_ptr is the preferred pointer to return from a factory function.
// https://www.fluentcpp.com/2017/08/25/knowing-your-smart-pointers/

// hm, shared_ptr can indeed lead to circular references...
// https://www.internalpointers.com/post/beginner-s-look-smart-pointers-modern-c
// but weak_ptr can prevent circular references?

// when to use unique_ptr
// https://stackoverflow.com/questions/106508/what-is-a-smart-pointer-and-when-should-i-use-one

// unique_ptr and arrays
// https://stackoverflow.com/questions/106508/what-is-a-smart-pointer-and-when-should-i-use-one

// for class members, better to use raw pointers or smart pointers?
// https://stackoverflow.com/questions/15648844/using-smart-pointers-for-class-members

// two ways to pass smart pointer to a function.
// https://stackoverflow.com/questions/30905487/how-can-i-pass-stdunique-ptr-into-a-function
//void MyFunc(unique_ptr<A>& arg) { cout << arg->GetVal() << endl; }
//int main(int argc, char* argv[]) { unique_ptr<A> ptr = unique_ptr<A>(new A(1234)); MyFunc(ptr); }
//void MyFunc(unique_ptr<A> arg) { cout << arg->GetVal() << endl; }
//int main(int argc, char* argv[]) { unique_ptr<A> ptr = unique_ptr<A>(new A(1234)); MyFunc(move(ptr)); assert(ptr == nullptr) }


#include <iostream>
#include <memory>
#include "tea_set.h"

using std::cout;
using std::endl;
using std::unique_ptr;


// for new-allocated arrays and objects, unique_ptr does automatic cleanup on scope exit.
// for malloc-allocated arrays, we must tell unique_ptr how to do cleanup on scope exit.
// https://www.codeproject.com/Articles/820931/Using-std-unique-ptr-RAII-with-malloc-and-free

class array_free { public: void operator() (int* p) { free(p); } };
	// don't understand this yet...

void show_cleanup_on_scope_exit()
{

	auto a = unique_ptr<int, array_free>{  
		reinterpret_cast<int*>(malloc(sizeof(int) * 10)) 
	};

	unique_ptr<int[]> b{ new int[10] };

	unique_ptr<tea_set> c{ new tea_set(3, 4) };

	cout << "about to leave show_cleanup_on_scope_exit scope" << endl;
}

void smart_pointers()
{
	show_cleanup_on_scope_exit();

	unique_ptr<int[]> arry{ new int[3] };

	// check if unique_ptr is still valid.
	if (arry) { 
		arry[2] = 14;
		cout << arry[2] << endl; 
	}

	cout << "todo" << endl;



}