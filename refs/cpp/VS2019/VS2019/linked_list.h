
// taken from:
// https://codereview.stackexchange.com/questions/185166/implementing-a-linked-list-in-c

// supports iterators and range-based for loop.

// placement-new
// https://en.cppreference.com/w/cpp/language/new#Placement_new

// emplace
// https://stackoverflow.com/questions/32926042/how-to-implement-a-simple-container-with-placement-new-and-emplace-functionality


#include <cstdint>
#include <iostream>
//#include <ostream>



template<class T>
class LinkedList final
{

public:
	LinkedList();

	~LinkedList();

	LinkedList(const LinkedList<T>& src);

	LinkedList& operator=(const LinkedList& src);

	LinkedList(LinkedList<T>&& src) noexcept;

	LinkedList& operator=(LinkedList&& src) noexcept;

	void swap(LinkedList& other) noexcept;

	void add(const T& value);

	void add(T&& value);
		// pass value by r-value reference allowing a move.
		// placement-new?

	template<class... Args>
	void add(Args&&... args);
		// pass parameters that allow you to build a T in place.
		// emplace?

	void remove(std::uint32_t index);

	T const& get(std::uint32_t index) const;   
		// get a const reference to the object.
	T& get(std::uint32_t index);         
		// get a reference to the object.

	std::uint32_t get_count() const;

	std::ostream& print(std::ostream& out) const;

	template <class Q>
	friend std::ostream& operator<<(std::ostream& out, const LinkedList<Q>& list);


private:
	struct ListNode
	{

		ListNode(const T value) : value_(value), next_(nullptr)
		{
		}

		~ListNode()
		{
		}

		ListNode* next_;
		T value_;
	};

	std::uint32_t count_;
	ListNode* start_;
	ListNode* end_;

public:
	struct iterator
	{
		typedef std::forward_iterator_tag iterator_category;
		typedef T value_type;
		typedef T* pointer;
		typedef T& reference;
		typedef size_t size_type;
		typedef ptrdiff_t difference_type;

		iterator(ListNode* init = nullptr) : current_(init) {}

		T& operator*()
		{
			return current_->value_;
		}

		const T& operator*() const
		{
			return current_->value_;
		}

		iterator& operator++()
		{ // prefix
			if (current_ != nullptr)
				current_ = current_->next_;
			return *this;
		}

		iterator operator++(int)
		{ // postfix
			iterator temp = *this;
			++* this;
			return temp;
		}

		bool operator==(const iterator& x) const
		{
			return current_ == x.current_;
		}

		bool operator!=(const iterator& x) const
		{
			return current_ != x.current_;
		}

	private:
		ListNode* current_;
	};

	iterator begin()
	{
		return iterator(start_);
	}

	iterator end()
	{
		return iterator();
	}
};

// default constructor
template<class T>
inline LinkedList<T>::LinkedList() : start_(nullptr), end_(nullptr), count_(0)
{
}

// destructor
template<class T>
inline LinkedList<T>::~LinkedList()
{
	// todo
}

// copy constructor
template<class T>
inline LinkedList<T>::LinkedList(const LinkedList<T>& src)
	: start_(nullptr), end_(nullptr), count_(0)
{
	for (auto node_iter = src.start_; node_iter != nullptr; node_iter = node_iter->next_)
	{
		Add(node_iter->value_);
	}
}

// copy assignment
template<class T>
inline LinkedList<T>& LinkedList<T>::operator=(const LinkedList& src)
{
	auto delete_iter = start_;

	while (delete_iter != nullptr)
	{
		start_ = start_->next_;
		delete delete_iter;
		delete_iter = start_;
	}

	for (auto node_iter = src.start_; node_iter != nullptr; node_iter = node_iter->next_)
	{
		Add(node_iter->value_);
	}

	return *this;
}

// move constructor
template<class T>
inline LinkedList<T>::LinkedList(LinkedList<T>&& src) noexcept
	: count_(0)
	, start_(nullptr)
	, end_(nullptr)
{
	swap(src);
}

// move assignment
template<class T>
inline LinkedList<T>& LinkedList<T>::operator=(LinkedList&& src) noexcept
{
	swap(src);
	return *this;
}

// swap?
template<class T>
void LinkedList<T>::swap(LinkedList& other) noexcept
{
	using std::swap;
	swap(count_, other.count_);
	swap(start_, other.start_);
	swap(end_, other.end_);
}

template<class T>
inline void LinkedList<T>::add(const T& value)
{
	if (start_ == nullptr)
	{
		start_ = new ListNode(value);
		end_ = start_;
	}
	else
	{
		end_->next_ = new ListNode(value);
			// calls the copy constructor?
		end_ = end_->next_;
	}

	count_++;
}

template<class T>
void LinkedList<T>::add(T&& value)
{
	// pass value by r-value reference allowing a move.
	if (start_ == nullptr)
	{
		start_ = new ListNode(value);
		end_ = start_;
	}
	else
	{
		end_->next_ = new ListNode(value);
			// calls the move constructor? (not implemented yet)
		end_ = end_->next_;
	}

	count_++;
}

template<class T>
template<class... Args>
	// so weird. why can't i do template<class T, class... Args> ?
	// discovered this fix completely by accident.
void LinkedList<T>::add(Args&&... args)
{
	// pass parameters that allow you to build a T in place.
}

template<class T>
inline void LinkedList<T>::remove(std::uint32_t index)
{
	if (index >= count_)
	{
		throw std::out_of_range("Index was larger than list length.");
	}

	count_--;

	if (index == 0)
	{
		auto old_start = start_;
		start_ = start_->next_;
		if (count_ == 1)
			end_ = nullptr;

		delete old_start;
		return;
	}

	auto current = start_;
	while (--index > 0)
	{
		current = current->next_;
	}

	auto next = current->next_;
	current->next_ = next->next_;

	if (end_ == next)
	{
		end_ = current;
	}

	delete next;
}

template<class T>
inline T const& LinkedList<T>::get(std::uint32_t index) const
{
	if (index >= count_)
	{
		throw std::out_of_range("Index was larger than list length.");
	}

	auto current = start_;
	while (index-- > 0)
	{
		current = current->next_;
	}

	return current->value_;
}

template<class T>
inline T& LinkedList<T>::get(std::uint32_t index)
{
	if (index >= count_)
	{
		throw std::out_of_range("Index was larger than list length.");
	}

	auto current = start_;
	while (index-- > 0)
	{
		current = current->next_;
	}

	return current->value_;
}

template<class T>
inline std::uint32_t LinkedList<T>::get_count() const
{
	return count_;
}

template<class T>
inline std::ostream& LinkedList<T>::print(std::ostream& out) const
{
	if (start_ == nullptr)
	{
		out << "[empty]" << std::endl;
		return out;
	}

	auto current = start_;
	while (current->next_ != nullptr)
	{
		out << current->value_ << " - ";
		current = current->next_;
	}

	out << current->value_ << std::endl;
	return out;
}

template<class T>
inline std::ostream& operator<<(std::ostream& out, const LinkedList<T> & list)
{
	list.print(out);
	return out;
}