
// template functions
// template classes

// template typename vs template class
// https://stackoverflow.com/questions/2023977/difference-of-keywords-typename-and-class-in-templates
// https://stackoverflow.com/questions/213121/use-class-or-typename-for-template-parameters?noredirect=1&lq=1

// why can templates only be implemented in the header file?
// (note: technically question is not correct, there are ways to implement templates outside of a header file)
// https://stackoverflow.com/questions/495021/why-can-templates-only-be-implemented-in-the-header-file?rq=1
// https://www.learncpp.com/cpp-tutorial/133-template-classes/
// "In order for the compiler to use a template, it must see both the template definition (not just a declaration)
// and the template type used to instantiate the template. Also remember that C++ compiles files individually."

// so how to resolve the template header issue?
// i think i prefer to just put all of the functionality in the header.


#include <iostream>

using std::cout;
using std::endl;

class foo
{
public:
	int x;
	foo(int p) :x{ p } {};
};

class bar
{
public:
	int y;
	bar(int q) :y{ q } {};
	int operator+(foo other) { return y + other.x; }
};

template <typename T, typename S>
bool positive(T x, S y)
{
	if (x + y > 0) return true;
	else return false;
}

template <typename T, typename S>
class baz
{
public:
	T* t;
	S* s;
	baz(int a, int b) :t{ new T{a} }, s{ new S{b} }{};
	~baz(){ delete t; delete s; }
	int sum() { return s->y + t->x; }
};

void templates()
{
	// use a function template
	foo f{ 3 };
	bar b{ 4 };
	cout << "foo plus bar is positive:" << positive(b, f) << endl;
	b.y = -4;
	cout << "foo plus bar is positive:" << positive(b, f) << endl;

	// use a class template
	baz<foo, bar> bz(7, -8);
	cout << bz.sum() << endl;

	cout << "bye" << endl;
}