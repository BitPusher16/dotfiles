
// STL containers
// https://www.learncpp.com/cpp-tutorial/16-2-stl-containers-overview/
// https://embeddedartistry.com/blog/2017/08/02/an-overview-of-c-stl-containers/
//   sequence containers
//     vector
//     deque
//     array
//     list
//     forward_list
//     basic_string
//   associative containers
//     set
//     multiset
//     map
//     multimap
//   container adapters
//     stack
//     queue
//     priority_queue
//   unordered associate
//     hash

// container time complexities
// https://stackoverflow.com/questions/181693/what-are-the-complexity-guarantees-of-the-standard-containers
// https://users.cs.northwestern.edu/~riesbeck/programming/c++/stl-summary.html

// idiomatic to use pair<> when inserting into hash, tree?
// http://www.cplusplus.com/reference/map/map/

// emplace vs insert
// https://stackoverflow.com/questions/14788261/c-stdvector-emplace-vs-insert

// push back vs emplace back (variadic templates not implemented in VC10?
// https://stackoverflow.com/questions/4303513/push-back-vs-emplace-back?noredirect=1&lq=1

// do containers automatically delete pointers that they contain?
// no, see vector_of_objects.cpp.

// why does this example recommend iterating with auto const& i?
// https://stackoverflow.com/questions/409348/iteration-over-stdvector-unsigned-vs-signed-index-variable
// "If you need only read access to the content, always use const iterators."
// https://softwareengineering.stackexchange.com/questions/370874/does-it-make-sense-to-iterate-a-ranged-for-loop-using-constant-reference-here

// simple operations on vectors
// https://medium.com/the-renaissance-developer/c-standard-template-library-stl-vector-a-pretty-simple-guide-d2b64184d50b


#include <algorithm>
#include <iostream>
#include <vector>
#include <deque>
#include <list>
#include <map>
#include <unordered_map>
#include <set>

using std::cout;
using std::endl;
using std::vector;
using std::sort;
using std::greater;
using std::deque;
using std::list;
using std::map;
using std::unordered_map;
using std::set;


void containers()
{
	// vector of ints

	vector<int> vec_a;
	for (int i = 0; i < 10; i++) vec_a.push_back(i);
	for (int i = 0; i <  vec_a.size(); i++) cout << vec_a[i] << ",";
	cout << endl;
	for (auto iter = vec_a.begin(); iter != vec_a.end(); iter++) cout << *iter << ",";
	cout << endl;

	// iterate with explicit iterator rather than auto
	for (vector<int>::iterator iter = vec_a.begin(); iter != vec_a.end(); iter++) { cout << *iter << ","; }
	cout << endl;

	// iterating with range
	for (auto i : vec_a) { cout << i << ","; }
	cout << endl;

	// size
	cout << "vec_a size:" << vec_a.size() << endl;

	// remove last
	vec_a.pop_back();

	// remove first
	vec_a.erase(vec_a.begin());
		// todo: don't understand this notation yet.

	// remove last with erase
	vec_a.erase(vec_a.begin() + vec_a.size() - 1);
		// todo: don't understand this notation yet.

	// get first
	cout << vec_a.front() << endl;

	// get last
	cout << vec_a.back() << endl;

	// size
	cout << vec_a.size() << endl;

	// check if empty
	cout << vec_a.empty() << endl;

	// access by index
	vec_a[3] = 22;
	cout << vec_a[3] << endl;

	// sort descending
	sort(vec_a.begin(), vec_a.end(), greater<int>());

	// sort ascending
	sort(vec_a.begin(), vec_a.end());

	// remove all
	vec_a.clear();



	// deque ("deck") of ints
	// deque has same time complexities as vec, but also has push_front() and pop_front().
	deque<int> deq_a;
	for (int i = 0; i < 3; i++) deq_a.push_back(i);
	for (int i = 10; i < 13; i++) deq_a.push_front(i);
	for (auto iter = deq_a.begin(); iter != deq_a.end(); iter++) cout << *iter << ",";
	cout << endl;

	deq_a.pop_front();
	deq_a.pop_back();
	for (auto iter = deq_a.begin(); iter != deq_a.end(); iter++) cout << *iter << ",";
	cout << endl;

	// list of ints
	// list keeps elements in a linked list rather than array. this means:
	// 1) list does not offer random access.
	// 2) inserting and deleting do not invalidate iterators pointed to other elements in the list

	list<int> list_a;
	for (int i = 0; i < 3; i++) list_a.push_back(i);
	for (auto iter = list_a.begin(); iter != list_a.end(); iter++) cout << *iter << ",";
	cout << endl;

	// map (key-value store, keys ordered)
	map<char, int> map_a;
	map_a['a'] = 1;
	map_a['c'] = 3;
	map_a['b'] = 2;
	for (auto iter = map_a.begin(); iter != map_a.end(); iter++) cout << iter->second << ",";
	cout << endl;

	map_a.erase('c');
	for (auto iter = map_a.begin(); iter != map_a.end(); iter++) cout << iter->second << ",";
	cout << endl;


	// unordered map (key-value store, keys unordered)
	unordered_map<char, int> umap_a;
	umap_a['a'] = 1;
	umap_a['c'] = 3;
	umap_a['b'] = 2;
	for (auto iter = umap_a.begin(); iter != umap_a.end(); iter++) cout << iter->second << ",";
	cout << endl;

	umap_a.erase('c');
	for (auto iter = umap_a.begin(); iter != umap_a.end(); iter++) cout << iter->second << ",";
	cout << endl;

	// set
	set<int> set_a;
	set_a.insert(1);
	set_a.insert(2);
	set_a.insert(2);
	for (auto iter = set_a.begin(); iter != set_a.end(); iter++) cout << *iter << ",";
	cout << endl;

	set<int> set_b{ 1, 2, 2, 3 };
	for (auto iter = set_b.begin(); iter != set_b.end(); iter++) cout << *iter << ",";
	cout << endl;





	cout << "stop" << endl;
}