

#include <iostream>
#include <string>

using std::cout;
using std::endl;
using std::string;
using std::to_string;

// in c++, structs and classes are identical,
// except that struct members are public by default,
// and class members are private by default.
// https://stackoverflow.com/a/54596/2512141

// most often, class methods are declared in a header file
// and defined in a .cpp file,
// but you can put both declaration and definition together if you wish.

// methods defined outside of a class use the class::method(){} syntax. (no semicolon needed)

// encapsulation: make class members private and use getters/setters.

// classes have 6 special member functions (why not called methods? i guess that's java nomenclature)
// if these functions are not defined, the compiler creates them implicitly.
// http://www.cplusplus.com/doc/tutorial/classes2/

// constructor (when compiler creates an implicit constructor, it creates an implicit default constructor.)
// destructor
// copy constructor
// copy assignment
// move constructor
// move assignment

// there is a bit of a dark art to initializing member classes.
// some best practice recommendations here:
// https://stackoverflow.com/questions/33683010/best-practice-for-class-member-initialization

// when provided arguments, constructors populate class members in the order they are declared in the class,
// not the order they are listed in the constructor param list.
// https://www.learncpp.com/cpp-tutorial/8-5a-constructor-member-initializer-lists/, initializer list order
// (this is only for implicit default constructors?)
// ah, i read the example a little more closely.
// i don't think this is talking about which values are mapped to which variables.
// i think this means that with two different orderings, the same values are mapped to the same vars,
// but the order in which those values are stored in the vars may differ.
// for example, a = 2, b = a + 3 will give different results than b = a + 3, a = 2, even though the same values are assigned.
// i'm not sure when this would come up...

// instantiating an object as const makes all members read-only.
// what about member classes?
// i would assume these are still modifiable, as member classes would be pointers.
// methods of a const object can only be called if they themselves are specified as const.
// http://www.cplusplus.com/doc/tutorial/templates/

// need an example where object is created with empty/populated arrays and vectors.

// copy constructors

// need an example of how = will cause copy from one object to another,
// and how to modify this so copied ptrs don't cause deallocation problems.

// would like an example of how default dtor automatically cleans up member classes.

// what happens if we create non-pointer, non-reference class member inside a class?
// https://stackoverflow.com/questions/3871429/class-members-that-are-objects-pointers-or-not-c

// see this for an example of a friend function that serializes an object for outputting with <<:
// https://www.learncpp.com/cpp-tutorial/15-2-rvalue-references/

// example linked list in c++ (asked in 2018)
// https://codereview.stackexchange.com/questions/185166/implementing-a-linked-list-in-c

// different ways to initialize a class (VS 2019)
// https://docs.microsoft.com/en-us/cpp/cpp/initializing-classes-and-structs-without-constructors-cpp?view=vs-2019

	// Member initialization (in order of declaration):
	//TempData td{ 45978, time(&time_to_set), 28.9, 37.0, 16.7 };

	// Default initialization = {0,0,0,0,0}
	//TempData td_default{};

	// Uninitialized = if used, emits warning C4700 uninitialized local variable
	//TempData td_noInit;

	// Member declaration (in order of ctor parameters) (TempData2 has an explicit constructor.)
	//TempData2 td2{ 16.7, 37.0, 28.9, 45978, time(&time_to_set) };


// https://medium.com/@winwardo/c-moves-for-people-who-dont-know-or-care-what-rvalues-are-%EF%B8%8F-56ee122dda7
// "If the type doesn't have special operations for moving, a move is just a copy.
//int a = 6;
//int b = std::move(a);

// "Do NOTE wrap your return value in std::move in a function.
// In many cases, this is actually slower than returning directly."

// "You can consider std::move(myString) to be loosely equivalent 
// to static_cast<String&&>(myString) � it casts from type T to type T&&."

// Author suggests exploring std::swap and RVO next.

// SO post on RVO:
// https://stackoverflow.com/questions/4986673/c11-rvalues-and-move-semantics-confusion-return-statement

// when does move constructor get called?
// https://stackoverflow.com/questions/13125632/when-does-move-constructor-get-called/13125851
// https://stackoverflow.com/questions/11077103/when-to-use-move-constructors-assignments
// https://stackoverflow.com/questions/10672763/is-this-why-the-move-constructor-in-c-11-makes-sense
// https://stackoverflow.com/questions/3413470/what-is-stdmove-and-when-should-it-be-used
// how about when calling a constructor and passing in a function call?
// foo(foobar()); // argument is rvalue: calls foo(X&&) // careful... this looks like a function declaration?

// http://thbecker.net/articles/rvalue_references/section_04.html
// finally, a concrete reason to implement move semantics: sorting a list of objects in place.
// but this calls move on lvalues, not rvalues? don't understand that part...
// also, why can't swap() just change the member values one-for-one (including pointers) in the swapped object?
// maybe only real valid use is unique_ptr?



class Vehicle
{
public:
	int num_wheels; 
	int num_seats;
	string fuel_type;

	// constructors
	Vehicle();
	//Vehicle(int);
		// had to remove this one because it gave Vehicle two default constructors.
		// see below.
	Vehicle(int, int, string);

	// method declared inside class, defined outside class
	string to_string();

	// method declared and defined inside class
	int get_wheels() { return num_wheels; }
};

// default constructor (no params, or all params have defaults)
Vehicle::Vehicle()
{
	num_wheels = 1;
	num_seats = 1;
	fuel_type = "pedal_power";
}

// this is also a default constructor?
// A: yes! tried to call this default constructor, got:
// error: class "Vehicle" has more than one default constructor
//Vehicle::Vehicle(int w=2)
//{
//	num_wheels = w;
//	num_seats = 1;
//	fuel_type = "pedal_power";
//}

// parameterized constructor
Vehicle::Vehicle(int w, int s, string f)
{
	num_wheels = w;
	num_seats = s;
	fuel_type = f;
}

// parameterized constructor, alternative syntaxes.
//Vehicle::Vehicle(int w, int s, string f): num_wheels(w), num_seats(s), fuel_type(f) {};
//Vehicle::Vehicle(int w, int s, string f) : num_wheels{ w }, num_seats{ s }, fuel_type{ f } {};

// parameterized constructor, invalide syntax.
//Vehicle::Vehicle(int w, int s, string f): num_wheels=w, num_seats=s, fuel_type=f {};

string Vehicle::to_string()
{
	// note that because we have specified that we are implementing a Vehicle method,
	// the Vehicle members are in scope.

	//return to_string(num_wheels) + "," + to_string(num_seats) + "," + fuel_type;
		// doesn't work! Vehicle::to_string has overridden std::to_string.

	using std::to_string;
	return to_string(num_wheels) + "," + to_string(num_seats) + "," + fuel_type;
}

class TrafficSign
{
public:
	// brace-or-equal initializers (so-called in the c++ standard).
	// default vals can be overwritten by a constructor.
	// https://arne-mertz.de/2015/08/new-c-features-default-initializers-for-member-variables/#Providing_default_values
	int num_sides = 8;
	int num_letters{ 4 };
	string color = "red";

	// note that this class has no explicit constructor,
	// yet a calling program can still pass arguments with uniform initialization, i.e. curly braces {}.
	// the class has an implicit constructor (correct name?)

	string to_string() { 
		using std::to_string;
		return "TrafficSign: " + to_string(num_sides) + "," + to_string(num_letters) + "," + color; 
	}
};

class RoadLine
{
public:

	int num_lines = 2;
	string line_color = "white";

	RoadLine(int nl, string lc)
	{
		cout << "initializing RoadLine" << endl;
		num_lines = nl;
		line_color = lc;
	}

	string to_string()
	{
		using std::to_string;
		return "RoadLine: " + to_string(num_lines) + "," + line_color;
	}

};

class Road
{
public:
	int num_lanes = 2;
	// CAUTION: Be careful when initializing a class member with a default object.
	// The RoadLine constructor will be called twice if we use the default constructor.
	// It will also be called twice if we initialize our own RoadLine and pass it to the parameterized constructor.
	RoadLine road_line{ 1, "yellow" };

	Road()
	{
		num_lanes = 2;

		road_line = RoadLine{ 2, "green" };
	}

	Road(int num_l, RoadLine rl)
	{
		num_lanes = num_l;
		road_line = rl;
	}

	string to_string()
	{
		using std::to_string;
		return "Road: " + to_string(num_lanes) + "," + road_line.to_string();
	}
};



class PotHole
{
public:
	bool closed;

	// with zero constructors, we would be allowed to call PotHole p_a;
	// however, if we define a constructor with params,
	// the implicit constructor is no longer created, and we cannot call PotHole p_a;
	PotHole(bool c) { closed = c; }
};


class TrafficLight
{
public:

	// TODO: should enums be covered here? are they ever used outside of classes?

	// this enum is not declared as "enum class".
	// this means the enum variables will be available in the same scope as the enum.
	// https://stackoverflow.com/a/18335862/2512141
	enum LightColor
	{
		GREEN, YELLOW, RED
	};

	// enum classes are newer and are now the preferred approach.
	// https://stackoverflow.com/a/18338930/2512141
	enum class LightShape
	{
		ROUND, ARROW
	};

	LightColor c = GREEN;
	LightShape sh = LightShape::ARROW;

	// a function that returns *this to allow function chaining.
	TrafficLight& next()
	{
		if (c == GREEN)
			c = YELLOW;
		else if (c == YELLOW)
			c = RED;
		else if (c == RED)
			c = GREEN;

		return *this;
	}

	string check()
	{
		switch (c)
		{
			case GREEN: return "green"; break;
			case YELLOW: return "yellow"; break;
			case RED: return "red"; break;
			default: return "error";
		}
	}
};

class WalkButton
{
public:
	bool requested = false;
	void press() { requested = !requested; };

	WalkButton() { cout << "constructing WalkButton" << endl; }
	~WalkButton() { cout << "destructing WalkButton" << endl; }
};

class Crosswalk
{
public:
	//WalkButton* b; // valid, but i want to try a reference instead of pointer.
	//WalkButton& b; 
		// nevermind, references can't be initialized inside a constructor. only in initialization list?
		// or use a reference_wrapper? https://stackoverflow.com/a/20233645/2512141
		// ah, even in initialization lists, default constructor is not allowed when assigning to a reference.
		// error: default initialization of reference is not allowed
		// too much trouble, going back to pointer.
	WalkButton* b;
	int num_stripes;

	Crosswalk()
	{
		cout << "constructing Crosswalk" << endl;
		b = new WalkButton;
		num_stripes = 8;
	}
	~Crosswalk() { 
		cout << "destructing Crosswalk" << endl; 
		delete b;
	}

	Crosswalk(const Crosswalk& obj)
	{
		cout << "copy constructing Crosswalk" << endl;
		b = new WalkButton;
		b->requested = obj.b->requested;
		num_stripes = obj.num_stripes;
	}
};

void classes()
{
	// copy initialization
	// only non-explicit constructor may be called.
	// https://stackoverflow.com/questions/51862128/initialization-with-empty-curly-braces
	// what does this mean?
	// foo f{}; vs foo f = {};
	// direct-list-initialization and copy-list-initialization.
	// https://stackoverflow.com/questions/51862128/initialization-with-empty-curly-braces
	// why explicit constructors are forbidden:
	// https://stackoverflow.com/questions/9157041/what-could-go-wrong-if-copy-list-initialization-allowed-explicit-constructors
	Vehicle veh_a = { 1, 2, "pedal_power" };

	// direct initialization (also called functional form?)
	// http://www.cplusplus.com/doc/tutorial/classes/
	Vehicle veh_b(1, 2, "pedal_power");

	// uniform initialization
	Vehicle veh_c{ 4, 5, "gasoline" };
	cout << veh_c.to_string() << endl;

	// (also)
	//Vehicle veh_c = {4, 5, "gasoline"};

	// default constructor
	//Vehicle veh_d();
		// wrong syntax, won't create an object.
	Vehicle veh_d;
	cout << veh_d.to_string() << endl;

	// what happens if you call a constructor but don't pass enough params?
	//Vehicle veh_e{ 4 };
		// error: no instance of constructor matches the argument list

	// what happens if you call a default constructor but pass args in the wrong order?
	//Vehicle veh_f{ 1, "foo", 2 };
		// error: no instance of constructor matches the argument list

	TrafficSign t_a;
	cout << t_a.to_string() << endl;

	TrafficSign t_b{ 8, 4, "red" };
	cout << t_b.to_string() << endl;

	Road r_a;
	cout << r_a.to_string() << endl;

	RoadLine rl_a{ 1, "white" };
	Road r_b{ 4, rl_a };
	cout << r_b.to_string() << endl;

	//PotHole p_a;
		// can't call this.
		// Pothole has a constructor with params, so the implicit constructor goes away.
	PotHole p_a(false);
	cout << to_string(p_a.closed) << endl;

	TrafficLight tl;
	cout << tl.check() << endl;
	cout << tl.next().next().check() << endl;

	Crosswalk c1;
		// calls default constructor for Crosswalk.
	Crosswalk c2 = c1;
		// usually, constructs Crosswalk c2 and copies members of c1 to c2.
		// CAUTION!! c2 would get a pointer to c1's WalkButton. 
		// both would try to call the destructor, and c2 would fail.
		// to prevent this, we have written a copy constructor for Crosswalk.
		// this gets called instead of the implicit constructor.


	// allocating on the stack vs allocating on the heap

	Vehicle veh_x = { 6, 8, "solar" };
		// alternatively, Vehicle veh_x{6, 8, "solar"};
		// allocated on the stack, dtor called when scope exits.
	Vehicle* veh_y = new Vehicle{ 8, 10, "wind" };
		// allocated on the heap, dtor called only when delete is called.
	Vehicle& veh_z = *new Vehicle{ 10, 12, "moonlight" };
		// allocated on the heap, dtor called only when delete is called.

	delete veh_y;
	delete& veh_z;
		// this is non-idiomatic? https://stackoverflow.com/questions/9286390/c-pointer-and-reference-with-new-keyword-when-instantiating

}