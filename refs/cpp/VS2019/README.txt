

C++ version: tested on ISO C++17.

/**********
* Resources:
**********/
https://www.learncpp.com/
http://www.cplusplus.com/doc/tutorial/ (has good examples of STL Containers, but must dig through site a bit)
	http://www.cplusplus.com/reference/map/map/
	http://www.cplusplus.com/reference/map/map/map/
https://en.cppreference.com/w/
https://www.tutorialspoint.com/cplusplus/index.htm
https://www.fluentcpp.com/
https://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines (Bjarne Stroustrup, Herb Sutter)
https://www.tutorialspoint.com/cpp_standard_library/index.htm (C++ Standard Library, including C Standard Library and C++ STL)
https://stackoverflow.com/questions/388242/the-definitive-c-book-guide-and-list/388282#388282 (Definitive C++ Book Guide and List)
https://github.com/AnthonyCalandra/modern-cpp-features#move-semantics C++20, C++17, C++14, C++11 added features
https://github.com/mortennobel/cpp-cheatsheet C++14 cheat sheet
https://isocpp.org/faq C++ super FAQ (combined FAQ's from Marshall Cline, Bjarne Stroustrup)
https://www.youtube.com/playlist?list=PLs3KjaCtOwSY_Awyliwm-fRjEOa-SRbs- Understanding C++ Through Lambdas (2019)
https://herbsutter.com/2020/02/23/references-simply/ References, simply. By Herb Sutter. (2020)
https://www.bogotobogo.com/cplusplus/object_returning.php full code for a C++ binary tree, good review of returning objects in 2020
https://news.ycombinator.com/item?id=23203699 Game Programming Patterns, free web book.
https://www.youtube.com/playlist?list=PLTEcWGdSiQenl4YRPvSqW7UPC6SiGNN7e Dive into C++11/14 (with game emphasis)

See C++ executed interactively: http://pythontutor.com/cpp.html#mode=edit

Multi-language design pattern reference.
https://sourcemaking.com/design_patterns/factory_method

Good explanation of static, dynamic, and import libraries here:
https://www.learncpp.com/cpp-tutorial/a1-static-and-dynamic-libraries/

Suggestion for project layout: https://api.csswg.org/bikeshed/?force=1&url=https://raw.githubusercontent.com/vector-of-bool/pitchfork/develop/data/spec.bs

Suggestions for header files: https://www.learncpp.com/cpp-tutorial/89-class-code-and-header-files/


/**********
* Comparison to Java
**********/

I have not yet found a good topological ordering of subjects for this reference.
Some topics will be mentioned/used before they are covered in a module.

General notes (having programmed most recently in Java):
In C++, functions can live outside of objects. These are called global functions.
C++ has no garbage collector. Memory management is manual. Memory leaks and circular references are possible.
C++ generally has lower memory usage than Java.
C++ templates correspond to Java generics.
C++ abstract classes correspond to Java interfaces.
	https://www.tutorialspoint.com/cplusplus/cpp_interfaces.htm

This Java code is invalid in C++:
	Box box = new Box();
The C++ "equivalent" is:
	Box* ptr_box = new Box();
Or, to allocate a box on the stack rather than heap:
	Box box();

Also, in Java, this creates a 2nd reference to an instantiated object:
	Box new_box = original_box;
But in C++, the same syntax makes a shallow copy of the original object:
	Box new_box = original_box;

/**********
* Defs:
**********/

I will do my best to adhere to the following.

Instantiated classes are called objects.
Classes have members (class variables) and methods (class functions).

Parameters are the variables named in a function definition,
Arguments are the values passed to parameters.

Member initializer list (also member initialization list): list of vars following the colon in a constructor.
Initializer list: used to initialize an array.
(not sold on these two. using defs found here: https://www.learncpp.com/cpp-tutorial/8-5a-constructor-member-initializer-lists/)

In C, you can pass a copy of a value or a pointer to a value. (Usually called pass-by-value, pass-by-reference, resp.)
In C++, there are copy, pointer, and reference. A reference is a pointer that is automatically dereferenced by the compiler.
So passing a copy is pass-by-value and passing a pointer or a reference is pass-by-reference.

An l-value is any value that has an address in memory. It can appear on the left side of an assignment statement. All variables are l-values.
An r-value is any value that can be assigned to an l-value. Variables, literals, and expressions are r-values. 
Note that all l-values are also r-values, but not all r-values are l-values.
https://www.learncpp.com/cpp-tutorial/611-references/

Plain Old C++ Objects (POCO), Plain Old Datatypes (POD)

ctor: constructor
cctor: copy constructor
dtor: destructor

syntax: rules for how the characters in code are translated to symbols, and how those symbols may be arranged.
	For example, x++; is composed of 3 symbols (x, ++, and ;) and that arrangement is syntactically valid.
	Syntactical analysis may be conducted by a combination of a lexer, which requires a lexer grammar and produces tokens,
	and a parser, which requires a parser grammar and produces an Abstract Syntax Tree.
semantics: rules for how symbols are translated into executable instructions, and whether those instructions are valid.
	For example, x++; will cause x to be incremented if x is an integer or a pointer, but it is invalid if x is a float.
	Semantic analysis requires knowledge of data types, available functions, and the acceptable input and output for those functions.
	Not all semantic errors can be caught at compile time.
https://stackoverflow.com/questions/17930267/what-is-the-difference-between-syntax-and-semantics-in-programming-languages

/**********
* Miscellaneous
**********/

Executables (but not libraries) must have a function called main(). It must not be part of a namespace.

Class members and method signatures are usually defined in a .h file.
Class methods are usually implemented in a .cpp file.

Including <string> (std::string, std::wstring, etc.) is different from including <string.h> (strcpy, strlen, other C-style string funcs).  

What Every Computer Scientist Should Know About Floating-Point Arithmetic:
https://docs.oracle.com/cd/E19957-01/806-3568/ncg_goldberg.html


Disabling address space layout randomization? https://stackoverflow.com/questions/9560993/how-do-you-disable-aslr-address-space-layout-randomization-on-windows-7-x64

A fundamental weakness of c++: no standardization at the binary level
https://stackoverflow.com/questions/5397447/struct-padding-in-c

Returning a struct creates a copy?
https://stackoverflow.com/questions/4570366/how-to-access-a-local-variable-from-a-different-function-using-pointers

In general, don't make classes with reference members. Use pointers instead.
// https://stackoverflow.com/questions/892133/should-i-prefer-pointers-or-references-in-member-data
One exception is if you want to enforce a policy that your object can't be created without an existing instance of another class.
Making this other class a member of current class and assigning to a reference will require the other class to exist beforehand. (?)

https://docs.microsoft.com/en-us/cpp/cpp/lvalues-and-rvalues-visual-cpp?view=vs-2019
"Every C++ expression has a type, and belongs to a value category."

lvalues, rvalues, 11-page explanation by Thomas Becker
http://thbecker.net/articles/rvalue_references/section_01.html

variadic templates, pretty function
https://eli.thegreenplace.net/2014/variadic-templates-in-c/
https://stackoverflow.com/questions/4384765/whats-the-difference-between-pretty-function-function-func

using variadic templates to hash multiple data types in a function.
http://lbrandy.com/blog/2013/03/variadic_templates/
why not just pass an array of void pointers? does c++ allow this?
yes, should. although in c++ you must cast a void pointer before assigning it?
https://eli.thegreenplace.net/2009/11/16/void-and-casts-in-c-and-c


ostream& operator<< (ostream& os, const tea_set& me) { return os << "tea_set" << id << "(" << "foo" << "," << "bar" << ")"; }
	// why can't i serialize cup and spoon?
	// https://stackoverflow.com/questions/16291623/operator-overloading-c-too-many-parameters-for-operation

std::size_t is not portable?
https://stackoverflow.com/a/409396/2512141


a few things introduced to C++11, from the intro to Effective Modern C++ by Scott Meyers
https://www.amazon.com/dp/1491903996/
"0 and typedefs are out, nullptr and alias declarations are in. 
Enums should now be scoped. 
Smart pointers are now preferable to built-in ones. 
Moving objects is normally better than copying them."

I really dislike the notation for passing variables by reference.
With pass-by-value, pass-by-reference in C, it is clear when you call the function
whether you are passing a copy or a pointer.
foo(x);
foo(&x);
With C++, passing by copy and passing by reference look the same.
foo(x);
foo(x);
You can only tell how the argument is treated by going and looking at the function.

https://www.cprogramming.com/tutorial/references.html
"Bjarne Stroustrup suggests that for arguments that the function is expected to change, 
using a pointer instead of a reference helps make this clear--
pointers require that the caller explicitly pass in the memory address."
Did he actually say this? Where?
Then why not just require some notation when passing in a reference?!

STL consists of 4 major parts:
1) containers
2) algorithms
3) iterators
4) functors
https://medium.com/@vgasparyan1995/how-to-write-an-stl-compatible-container-fc5b994462c6

some relatively clean definitions of rvalue, xvalue, etc:
https://isocpp.org/blog/2016/04/quick-q-what-are-rvalues-lvalues-xvalues-glvalues-and-prvalues
"An lvalue designates a function or object."
An xvalue also refers to an object, usually near the end of its lifetime.
A glvalue is an lvalue or xvalue.
An rvalue is an xvalue, temporary object, or subobject thereof, or a value not associated with an object.
A prvalue is an rvalue that is not an xvalue.
(What does "object" mean in these definitions?)
http://www.stroustrup.com/terminology.pdf

https://www.fluentcpp.com/2018/02/06/understanding-lvalues-rvalues-and-their-references/ (2018)
an lvalue reference binds to an lvalue.
an rvalue reference binds to an rvalue.
exception: a const lvalue reference can bind to an rvalue. (not sure why this is useful.)
rvalue references are seen most often as function params in templates and move constructors.

unique, shared, weak pointers in c++, sep 2019
control block can't be released until last weak pointer is gone?
https://www.youtube.com/watch?v=XH4xIyS9B2I
also, make_shared with weak pointers may prevent object memory from being freed...
good grief, this is a mess.
also, if you make two shared pointers for same object (without using make_shared),
both pointers think they are unique owner? 27:38
price of shared pointers in multi-threaded programs, 42:28
so we should pass shared pointers by reference?
52:26: "If you can use unique pointers, use them. A shared pointer is a smell."

herb sutter, leak-freedom in c++, by default.
https://www.youtube.com/watch?v=JfmTagWcqoE
37:23, don't own upwards
40:08, how leaks can still happen in Java and C# (callbacks)
pdf
https://github.com/CppCon/CppCon2016/blob/master/Presentations/Lifetime%20Safety%20By%20Default%20-%20Making%20Code%20Leak-Free%20by%20Construction/Lifetime%20Safety%20By%20Default%20-%20Making%20Code%20Leak-Free%20by%20Construction%20-%20Herb%20Sutter%20-%20CppCon%202016.pdf

Herb Sutter, Back to the Basics! (C++14)
https://www.youtube.com/watch?v=xnqTKD8uD64
slides: https://github.com/CppCon/CppCon2014/blob/master/Presentations/Back%20to%20the%20Basics!%20Essentials%20of%20Modern%20C%2B%2B%20Style/Back%20to%20the%20Basics!%20Essentials%20of%20Modern%20C%2B%2B%20Style%20-%20Herb%20Sutter%20-%20CppCon%202014.pdf

Range-based for loops:
for(auto& e : c){... use(e); ... } // early break not supported yet?
for(e:c){... use(e); ...} // available in C++17?

"Raw * and & are great!"
Hm, changed his tune in 2020? https://www.youtube.com/watch?v=qx22oxlQmKc


herb sutter, writing good C++14 by default
https://www.youtube.com/watch?v=hEx5DNLWGgA
pdf:
https://github.com/isocpp/CppCoreGuidelines/blob/master/talks/Sutter%20-%20CppCon%202015%20day%202%20plenary%20.pdf




shared pointer aliasing
https://stackoverflow.com/questions/27109379/what-is-shared-ptrs-aliasing-constructor-for

static keyword does different things depending on where it is used.
https://docs.microsoft.com/en-us/cpp/cpp/storage-classes-cpp?view=vs-2019

two data structure code reviews:
https://codereview.stackexchange.com/questions/214850/implementing-a-binary-tree-in-c-using-stdunique-ptr
https://codereview.stackexchange.com/questions/190123/binary-search-tree-c?rq=1



C++ Most Vexing Parse
http://www.cplusplus.com/forum/general/83328/
https://en.wikipedia.org/wiki/Most_vexing_parse
https://www.fluentcpp.com/2018/01/30/most-vexing-parse/

pretty good discussion of move semantics, unique_ptr, what was wrong with auto_ptr, etc.
https://stackoverflow.com/questions/3106110/what-is-move-semantics
"C++98 rvalues are known as prvalues in C++11. Mentally replace all occurrences of "rvalue" in the preceding paragraphs with "prvalue"."
Moving into functions, and out of functions.

read the "Rvalue references (C++11)" section here:
https://eli.thegreenplace.net/2011/12/15/understanding-lvalues-and-rvalues-in-c-and-c/
also try https://www.cprogramming.com/c++11/rvalue-references-and-move-semantics-in-c++11.html
and https://www.learncpp.com/cpp-tutorial/15-1-intro-to-smart-pointers-move-semantics/

Oh interesting! Calling "this" without any operators gives a memory location.
https://eli.thegreenplace.net/2011/12/15/understanding-lvalues-and-rvalues-in-c-and-c/, in section Rvalue References.


//point_2d b;
//b.x = 8;
	// Interesting... If we assign nothing to b, compiler complains about uninitialized.
	// But assigning to only one of its members removes the error...

Interesting. Visual Studio compiler (MSVC) does not allow disabling of copy elision.
https://stackoverflow.com/questions/9941043/how-to-disable-return-value-optimization-in-visual-studio-2010

I like this example of what function definitions say about the intent of a function.
https://stackoverflow.com/a/37953536/2512141

//Here, let me buy you a new car just like mine. I don't care if you wreck
//it or give it a new paint job; you have yours and I have mine.
void foo(Car c);

//Here are the keys to my car. I understand that it may come back...
//not quite the same... as I lent it to you, but I'm okay with that.
void foo(Car& c);

//Here are the keys to my car as long as you promise to not give it a
//paint job or anything like that
void foo(const Car& c);

//I don't need my car anymore, so I'm signing the title over to you now.
//Happy birthday!
void foo(Car&& c);

Interesting. Run-time dynamic linking allows branching to load different modules depending on program needs.
Also, with run-time dynamic linking, no import library file is required. Load-time dynamic linking needs an import library file (will exist as .lib).
https://support.microsoft.com/en-us/help/815065/what-is-a-dll

Hm, lvalue references allow writing generic template code which handles values and references (pointers) identically?
http://www.cplusplus.com/articles/z6vU7k9E/

template< typename T >
void my_swap( T& t1, T& t2 ) 
{
    T tmp( t1 );
    t1 = t2;
    t2 = tmp;
}

int i = 42, j = 10;
int& iref = i, jref = j;

my_swap( i, j );          // Sets i = 10 and j = 42
my_swap( iref, jref );    // Swaps i and j right back 


https://www.fluentcpp.com/2018/07/17/how-to-construct-c-objects-without-making-copies/
"So how do we make a class that holds a reference if given an lvalue, but moves (and owns) when given rvalues?"


There's a very strange disconnect happening with shared pointers that I haven't untangled yet.
For data on heap, use unique_ptr by default. But never pass to function because the reference counter arithmetic is a performance hit?
https://www.youtube.com/watch?v=UsrHQAzSXkA
slides: https://channel9.msdn.com/Events/Build/2018/BRK2146
slide 30:
"Never pass smart pointers (by value or by reference) unless you actually want to manipulate the pointer (store, change, or let go of a reference)."
Then how can smart pointers be useful in data structure classes?

How to write a C++ container?
https://en.cppreference.com/w/cpp/container (see function list for associative containers, <map>)
There are 3 classes of container: sequence, associative, unordered associative
https://en.cppreference.com/w/cpp/header/map
https://en.cppreference.com/w/cpp/memory/allocator
with c++17, std::allocator interface becomes easier to handle:
https://www.modernescpp.com/index.php/memory-management-with-std-allocator



https://stackoverflow.com/questions/7758580/writing-your-own-stl-container
near-final draft of C++11 standard: (see Ch 23)
http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2011/n3242.pdf


"initializer list" and std::initalizer_list<> are 2 separate things.
std::initializer_list<> is a way to deal with "initializer lists"
https://youtu.be/7DTlWPgX6zs?t=2191
also, 19 ways to initialize an int ??!
https://youtu.be/7DTlWPgX6zs

To add an object to vector without copy, either use emplace_back (object must have a constructor)
or use move semantics to pass an rvalue reference. (2012. Still accurate?)
https://stackoverflow.com/questions/10488348/add-to-vector-without-copying-struct-data

Ah, yuck.
Example e = 10;
e = 20;
These call different xtors for class Example.
First one calls constructor, second calls copy assignment operator.
https://www.youtube.com/watch?v=tPbrWAbzyTE&list=PLTEcWGdSiQenl4YRPvSqW7UPC6SiGNN7e&index=2
(2013, still accurate?)

When to use inheritance vs interfaces. (Java)
https://www.javaworld.com/article/3409071/java-challenger-7-debugging-java-inheritance.html 
Note that in C++, interfaces are implemented using abstract classes.
https://www.tutorialspoint.com/cplusplus/cpp_interfaces.htm
I suppose the distinction is that inheritance (function implemented) allows child classes to share code,
while interface (virtual function) only requires "children" to share function signatures.
Note that under hungarian notation, an "abstract" class has both virtual and pure virtual functions. Not sure if this naming is standard.
https://www.xoax.net/cpp/ref/core/incl/hungarian_notation/

Virtual vs pure virtual:
Virtual functions are declared with virtual keyword. They have an implementation in the base class and can be overridden.
Pure virtual functions have virtual keyword and = 0. They do not have implementations in the base class. Adding a pure virtual function to a class makes it abstract.
https://stackoverflow.com/questions/2652198/difference-between-a-virtual-function-and-a-pure-virtual-function

Interesting. Prior to C++11, initializing class members could only be done in the constructor.
Now, class members can be initialized at the same place they are declared.
https://youtu.be/tPbrWAbzyTE?t=1034

stack/heap is also called automatic/dynamic storage?
https://www.youtube.com/watch?v=0TGp0o1KnG8&list=PLTEcWGdSiQenl4YRPvSqW7UPC6SiGNN7e&index=3
Vittorio Romeo

As of 2014, Clang and GCC offered int overflow detection, but VC++ did not.
https://stackoverflow.com/questions/199333/how-do-i-detect-unsigned-integer-multiply-overflow/199455#199455

best not to alias unique_ptr?
https://www.fluentcpp.com/2019/01/22/pitfalls-of-aliasing-a-pointer-in-modern-cpp/

components-entities method of game object management (better than class hierarchies)
https://youtu.be/QAmtgvwHInM?t=819

functions defined in classes are implicitly inline, and therefore won't cause multiple definition errors if used by multiple files.
https://stackoverflow.com/questions/2225435/how-do-i-create-a-header-only-library

is it possible to release a header-only library as a .lib or .dll?
what if the header-only library includes templates?

https://stackoverflow.com/questions/25606736/library-design-allow-user-to-decide-between-header-only-and-dynamically-linke

#if defined(INLINE_LIBRARY)
#define LIBRARY_API inline
#elif defined(STATIC_LIBRARY)
#define LIBRARY_API
#elif defined(EXPORT_LIBRARY)
#define LIBRARY_API __declspec(dllexport)
#elif defined(IMPORT_LIBRARY)
#define LIBRARY_API __declspec(dllimport)
#endif

Why must we put templates entirely in header files?
https://stackoverflow.com/questions/115703/storing-c-template-function-definitions-in-a-cpp-file?rq=1
https://stackoverflow.com/questions/495021/why-can-templates-only-be-implemented-in-the-header-file

2020-06-03
I think I realized today why C++ keeps the deep ugliness of template headers even today.
If you wish to write a container which stores and retrieves arbitrary objects, you must either:
1) use a template and compile specifically for that object type, or
2) store pointers to objects.
Java, and probably python and other "nice" languages use 2). C++ avoids 2), because it requires an extra dereference and is thus less performant.
So, if you are writing a C++ library which has fixed data types (e.g., OpenCV, GDAL, etc.), split code between headers and code files.
On the other hand, if you are writing templated data structures, use header-only.
I don't know how modules will attempt to tackle this...
If I am correct, then precompiled headers will not resolve the header vs library issue. (?)

And you can't get around this issue: Templates can't accommodate virtual classes. (?)
https://stackoverflow.com/questions/3251549/creating-an-interface-for-an-abstract-class-template-in-c?rq=1

In microsoft's implementation of the Standard Template Library, hashmap has a header only, and no .cpp
https://github.com/microsoft/STL/tree/master/stl/src
Same for vector and unordered_map.

GrammaTech approach to header/code/template problem
https://blogs.grammatech.com/separate-compilation-templates

https://stackoverflow.com/questions/34652029/how-should-i-write-my-c-to-be-prepared-for-c-modules

C++ modules and cmake (not sure about the date)
https://indico.cern.ch/event/848967/contributions/3567323/attachments/1912138/3159938/C_Modules_and_CMake.pdf

cmake c++ modules sandbox (examples of how to use modules)
https://github.com/mathstuf/cxx-modules-sandbox/tree/master/simple

c++ modules, pre-build scan needed now.
https://www.reddit.com/r/cpp/comments/e2c1ca/c20_modules_and_build_performance/

https://www.reddit.com/r/cpp/comments/e2c1ca/c20_modules_and_build_performance/

cmake: static, shared, and header-only libraries
https://www.markusrothe.dev/Static-Shared-Header-Only-Libraries/


Passing by const ref is "by far the most common case in ordinary good code".
Tour of C++, 2nd, section 3.6.1

Sec 3.6.2 shows returning a ref to the ith element of an array?
What happens if item is modified?
Eh, wait, nevermind. This would just be a pointer to an array element.
It is returning references to vector<> elements that is dangerous?

Very odd example in sec 3.6.2...
Function declares x, returns x, but author says compiler will complain.
Note: This is only because function signature shows return type int&. Why would you ever do this?

Page 66, return large matrix using move constructor rather than return a pointer (which must be deleted later by caller).
Points to section 5.2.2

sec 3.6.3, recommends returning large/complicated objects by returning structs? why not modify references?

RAII is explained in 4.2.2?
Resource Acquisition is Initialization. (But RAII usually also refers to the practice of automatically cleaning up when leaving scope.)
"The technique of acquiring resources in a constructor and releasing them in a destructor, known as RAII..."

