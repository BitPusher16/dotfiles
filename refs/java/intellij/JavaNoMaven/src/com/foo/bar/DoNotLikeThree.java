package com.foo.bar;

public class DoNotLikeThree {
    static void DislikeThreeChecked(int a) throws Exception{
        if(a == 3){ throw new Exception("You gave me a three! You must handle this debacle in your calling code."); }
    }

    static void DislikeThreeUnchecked(int a){
        if(a == 3){ throw new RuntimeException("You gave me a three! Catch or let the program crash, I don't care."); }
    }
}
