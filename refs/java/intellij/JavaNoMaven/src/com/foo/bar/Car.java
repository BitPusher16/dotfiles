package com.foo.bar;

public class Car extends WheeledVehicle {

    public Car(){
        super(4, 4);
    }

    public String toString(){
        return "Car: " + super.toString();
    }

    public void turnKey(){
        System.out.println("vroom!");
    }
}
