package com.foo.bar;

import java.util.ArrayList;

// BLE: Biggest Less than or Equal to
// SGE: Smallest Greater than or Equal to

public class ArrayListUtils {

    static <T extends Comparable> int binarySearch(ArrayList<T> arry, T key, String dir){
        return binarySearch(arry, key, dir, 0, arry.size()-1);
    }

    static <T extends Comparable> int binarySearch(ArrayList<T> arry, T key, String dir, int beg, int end){
        System.out.println("searching from index " + beg + " to " + end);
        if(beg == end){
            if(arry.get(beg).compareTo(key) == 0){
                return beg;
            }
            else if(arry.get(beg).compareTo(key) < 0){
                // curr < key
                if(dir.equals("BLE")){
                    return beg;
                }
                else{ return beg == arry.size() - 1 ? -1 : beg + 1; }
            }
            else{
                // curr > key
                if(dir.equals("SGE")){
                    return beg;
                }
                else{ return beg == 0 ? -1 : beg - 1; }
            }
        }

        int mid = (end - beg + 1) / 2 + beg;
        if(arry.get(mid).compareTo(key) == 0){
            return mid;
        }
        else if(arry.get(mid).compareTo(key) > 0){
            return binarySearch(arry, key, dir, beg, mid - 1);
        }
        else return binarySearch(arry, key, dir, mid, end);
    }
}
