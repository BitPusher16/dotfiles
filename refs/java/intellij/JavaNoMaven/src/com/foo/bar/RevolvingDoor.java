package com.foo.bar;

public class RevolvingDoor extends Door {
    boolean pinDropped;

    public RevolvingDoor(){
        pinDropped = false;
    }

    public void togglePin(){
        if(!pinDropped){
            System.out.println("dropping pin");
        }
        else{
            System.out.println("raising pin");
        }
        pinDropped = !pinDropped;
    }

    public void makeSecure(){
        System.out.println("making revolving door secure");
        if(!pinDropped){ togglePin(); }
        else{ System.out.println("revolving door is already secure"); }
    }
}
