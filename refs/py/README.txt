

As recently as 2020-02-23, following instructions would let you install a non-interfering
Python binary on your Windows machine.
- As of 2020-02-23, Windows is still listing a python executable as an alias. 
	It masks your other installations.
	To uninstall, Win key -> App execution aliases and disable both python checkboxes.
	Really hoping they revert this soon...
- Get a Python 64-bit exe installer from python.org.
- DON'T use default installations, it will do a global install.
- Untick "Install launcher for all users"
- Leave "Add to PATH" unchecked.
- Do "Customize installation".
- Uncheck "py launcher" and "for all users"
- Uncheck "Create shortcuts for installed applications"
- Choose path where you want python exe's and tcl/tk

Installation is done.
Select Python version by adding to PATH environment variable.

Creating and activating a venv (Python docs recommended way of installing a virtual env).
PS> python -m venv .\my_venv_dir
PS> .\my_venv_dir\Scripts\Activate.ps1
PS> cd C:\my\python\project\dir
PS> python -m pip install -r requirements.txt

To open a python project in Pycharm, right-click directory containing .idea folder and select 
"Open Folder as Pycharm Project"