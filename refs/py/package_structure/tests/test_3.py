
import unittest
from package_structure.foo.foo_funcs import foo_func_baz
from package_structure.bar.use_foo import baz_bip

class TestFooMethod(unittest.TestCase):

    def test_foo(self):
        self.assertEqual(3, 3)

    def test_foo_func(self):
        self.assertEqual(foo_func_baz(), 'baz')

    def test_foo_func_fail(self):
        self.assertEqual(foo_func_baz(), 'baz')

    def test_bar_func(self):
        self.assertEqual(baz_bip(), 'baz bip')