
=====

CAUTION: If trying to call this package as a module from PowerShell, do NOT use tab autocomplete. E.g.:

PS> python -m .\package_structure\

Prepending with a '.' will mess up Python's package navigation. Instead, do:

PS> python -m package_structure

=====

To use this package, create a python virtual environment and activate it.

PS> python -m venv ./my_env_dir
PS> cd my_env_dir/Scripts
PS> ./Activate.ps1

Then cd into package root dir and do:

PS> python -m package_structure

=====



https://realpython.com/python-application-layouts/