#include <iostream>
#include <dragnd/bar/bar.hpp>
#include <dragnd/baz/baz.hpp>
#include <dragnd/bif/bif.hpp>

using std::cout;
using std::endl;

using dragnd::bar::bar_func;
using dragnd::baz::baz_func;
using dragnd::bif::bif_func;

void check(bool b) { if (!b) { exit(1); } }

int main(int argc, char** argv)
{
	cout << "running test bur" << endl;
	check(bar_func(2) == 20);
	check(baz_func(2) == 14);
	check(bif_func(2) == 18);
	cout << bif_func(2) << endl;
	return 0;
}