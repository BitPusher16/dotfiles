#ifndef DRAGND_BAR
#define DRAGND_BAR

// This #include cannot go inside namespace. Why?
#include <array>

namespace dragnd { 
namespace bar {

int bar_func(int x) { return x * 10; }

template <typename T>
class circular_buffer
{
private:

	std::array<T, 4> arr;
	int next_consume;
	int next_insert;

public:

	circular_buffer()
	{
		arr = {};
		next_consume = 0;
		next_insert = 0;
	}

	bool can_consume()
	{
		if (next_consume == next_insert) { return false; }
		return true;
	}

	bool can_insert()
	{
		if (((size_t)next_insert + 1) % arr.size() == next_consume) { return false; }
		return true;
	}

	void insert(T x)
	{
		arr[next_insert] = x;
		next_insert = ((size_t)next_insert + 1) % arr.size();
	}

	void consume(T& r)
	{
		int tmp = next_consume;
		next_consume = ((size_t)next_consume + 1) % arr.size();
		r = arr[tmp];
	}

};

}}

#endif