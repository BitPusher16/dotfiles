#ifndef DRAGND_BAZ
#define DRAGND_BAZ

// Header guards only prevent multiple declarations within a single compilation unit?
// Will not prevent conflicts if same header is imported by multiple libraries?

namespace dragnd { 
namespace baz {

int baz_func(int x);

}}

#endif