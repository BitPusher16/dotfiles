#ifndef DRAGND_BIF
#define DRAGND_BIF

// Make dynamic libraries "MSVC compatible".
// https://gernotklingler.com/blog/creating-using-shared-libraries-different-compilers-different-operating-systems/
// This variable is defined automatically by cmake when we specify bif as shared? can I preface it with a namespace somehow?
#ifdef _WIN32
	#ifdef bif_EXPORTS 
		#define bif_API __declspec(dllexport)
	#else
		#define bif_API __declspec(dllimport)
	#endif
#else
#define bif_EXPORTS
#endif

namespace dragnd { 
namespace bif {

// When building library as dynamic, bif_API is only needed in the declaration? 
// https://stackoverflow.com/questions/557317/is-declspecdllexport-needed-in-cpp-files
int bif_API bif_func(int x); 

}}

#endif