#include <iostream>
#include <dragnd/bar/bar.hpp>
#include <dragnd/baz/baz.hpp>
#include <dragnd/bif/bif.hpp>

using std::cout;
using std::endl;
using dragnd::bar::bar_func;
using dragnd::bar::circular_buffer; // A templated data structure
using dragnd::baz::baz_func;
using dragnd::bif::bif_func;

// A binary doesn't need its own header because it exports nothing that other code will use. (?)
// For the same reason, a binary doesn't need a namespace. (?)

int main(int argc, char** argv)
{
	cout << "hi" << endl;
	cout << bar_func(12) << endl;
	cout << baz_func(3) << endl;
	cout << bif_func(8) << endl;

	circular_buffer<double> c;
	double d = 3.0;
	double s;
	while (c.can_insert()) { c.insert(d); d++; }

	circular_buffer<double> f = c;
	f.consume(s);
	f.insert(20.0);

	while (c.can_consume()) { c.consume(s); cout << s << endl; }
	while (f.can_consume()) { f.consume(s); cout << s << endl; }
	return 0;
}