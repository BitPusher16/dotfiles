
// Does this belong in /binary_tree_cmake_boost/include/binary_tree_cmake_boost?


#include <iostream>

// https://www.boost.org/doc/libs/1_72_0/doc/html/program_options.html
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/parsers.hpp>

#include "bin_tree.hpp"
#include "header_only.hpp"
#include "cmake_configure.hpp"

using std::cout;
using std::cerr;
using std::endl;

namespace po = boost::program_options;