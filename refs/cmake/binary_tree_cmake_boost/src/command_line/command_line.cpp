
#include "command_line.hpp"

int main(int argc, char** argv)
{
	cout << "hello" << endl;
	cerr << triple(8) << endl;

	po::options_description desc("Allowed options");
	desc.add_options()
		("help", "displays help")
		("compression", po::value<int>(), "sets compression level")
		("version", "shows binary_tree version and version of cmake used to build")
	;
	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);

	if (vm.count("help"))
	{
		cout << desc << endl;
		return 1;
	}

	if (vm.count("compression"))
	{
		cout << "compression set to " << vm["compression"].as<int>() << endl;
	}

	if (vm.count("version"))
	{
		cout << "binary tree version=" << TREE_VERSION << "-" << TREE_FLAVOR << " cmake version=" << CMAKE_VERSION << endl;
	}


	cout << "goodbye" << endl;

}