
# this is required for ctest?
# https://www.boost.org/doc/libs/1_62_0/libs/test/doc/html/boost_test/section_faq.html#boost_test.section_faq.how_to_set_up_a_cmake_project_us

# putting enable_testing() here doesn't work for some reason...
# needs to go in top-most CMakeLists, right after project declaration.
#enable_testing()

add_executable(test_1 test_1.cpp)
target_include_directories(test_1 PUBLIC ${Boost_INCLUDE_DIRS})
add_test(NAME run_test_1 COMMAND test_1)

add_executable(test_2 test_2.cpp)
target_include_directories(test_2 PUBLIC ${Boost_INCLUDE_DIRS})
target_link_libraries(test_2 PRIVATE header_only)
add_test(NAME run_test_2 COMMAND test_2)


