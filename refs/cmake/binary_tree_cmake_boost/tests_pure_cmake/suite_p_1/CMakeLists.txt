

add_executable(cmake_test_1 cmake_test_1.cpp)
target_link_libraries(cmake_test_1 PRIVATE header_only)

#add_test(NAME yes_it_runs COMMAND cmake_test_1) # Program expects an integer argument. This test will fail.
add_test(NAME yes_it_runs COMMAND cmake_test_1 18) # Succeeds if program returns 0.

add_test(NAME triple_works COMMAND cmake_test_1 12)
set_tests_properties(triple_works PROPERTIES PASS_REGULAR_EXPRESSION "36")