

##########
# References
##########

CMake 3.17 docs:
https://cmake.org/cmake/help/v3.17/index.html
CMake 3.17 docs example repo:
https://github.com/Kitware/CMake/tree/master/Help/guide/tutorial

This repo attempts to adhere to the following guide as closely as possible.
https://cliutils.gitlab.io/modern-cmake/chapters/basics/structure.html
https://cliutils.gitlab.io/modern-cmake/
https://gitlab.com/CLIUtils/modern-cmake
https://www.linkedin.com/in/henryiii/

Found a goldmine! Examples, plus lots of links to other CMake resources.
https://github.com/pr0g/cmake-examples#modern-cmake-examples
https://www.youtube.com/watch?v=6sWec7b0JIc&feature=youtu.be
Hm... Discusses how Visual Studio doesn't honor the CMAKE_BUILD_TYPE variable?

##########
# Building this project
##########

From command line:
PS> cd binary_tree_cmake_boost # Note: The root dir. Not include/binary_tree_cmake_boost.
PS> mkdir build
PS> mkdir install
PS> cd build
PS> cmake .. -D CMAKE_INSTALL_PREFIX=..\install   # variable CMAKE_INSTALL_PREFIX and location of root CMakeLists.txt will be cached in CMakeCache.txt
PS> cmake --build . # reads .\CMakeCache.txt
PS> ctest -C Debug   # -C Debug is required on Windows, when building with Visual Studio (which is a multi-config generator)
PS> cmake --build . --target install
PS> cd ..
PS> rmdir build -recurse
PS> rmdir install -recurse

From Visual Studio:
To build with visual studio, navigate to folder holding root CMakeLists.txt file (also contains .gitignore), right click, and select "Open in Visual Studio".
Visual studio will detect CMakeLists.txt, create directory .\out\build, and do cmake configuration. (Building/installing can then be done within Visual Studio.)

##########
# Types of C++ projects
##########

What are possible outputs of a C++ project?

For running:
- Standalone executable (.exe).
- Executable (.exe) which needs dynamic library (.dll) present at execution.
For building:
- Static library (.lib) for incorporating in another C++ project. Requires header files (.hpp) to integrate.
- Dynamic library (.dll) for incorporating in another C++ project. Requries header files (.hpp) to integrate. May require import library (.lib) as well.
- Header-only (.hpp) library for consumption in another C++ project.
- Uncompiled source for consumption in another C++ project. (.cpp, .hpp).

What is the best approach for installing or sharing each of these output types?
Approach taken by OpenCV:
- install/include contains tree of header files
- install/x64/vc16/bin contains flat list of .dll's, d.dll's, and a few .exe's.
- install/x64/vc16/lib contains flat libs .lib's and d.lib's.
(actually, only d.dll's and d.lib's are present. I must have built debug configuration?)

The simplest install is to create install dir and put all headers in install/include, all .lib's in install/lib, and all .dll's and .exe's in install/bin.
But it could make sense to nest the header files (for easier includes while coding), and also to nest bin and lib dirs within proc_architecture/compiler/release_configuration structure.
In this advanced case, surfacing .lib, .dll, .exe files to single folder is easy with CMake. But how to generate nested header file structure?

##########
# Project overview
##########


This example shows how to set up a directory structure for developing with CMake and Boost.

The example contains code and tests for a simple binary tree library.

Running this example requires a working cmake binary on your machine. It also requires Boost, with static libraries built.

The tests should generate data, then test using that data.
(For generating data, investigate add_custom_command() https://cmake.org/cmake/help/latest/manual/cmake-buildsystem.7.html#binary-executables)

CMake build->install should compile the project, run tests, and create /include, /lib, and /bin in the "out" or "build" directory.

The generated /include dir should have structure: /include/[PROJECT_NAME]/public_header(s).h

Note that CMakeSettings.json, out/build, and out/install are all created by opening this project in Visual Studio.

Interesting... If I add an x64 Release build config in Visual Studio, it uses RelWithDebInfo by default.
I read somewhere this is the preferred setting for building release with CMake?

##########
# Todo
##########

Still todo:
- Figure out how to compile, run, and validate output of data generators using unit tests.
    Start here and navigate right to get to data generation with Boost Test:
    https://www.boost.org/doc/libs/1_72_0/libs/test/doc/html/index.html
    https://www.boost.org/doc/libs/1_72_0/libs/test/doc/html/boost_test/tests_organization.html
    Hmm... Seems these are for testing one "row" at a time?
    Maybe I can use a fixture to generate data on disk?
    Should I be looking for integration testing instead of unit testing?
- Figure out how Debug, Release targets work.
- Figure out how to make project installable in another CMake project.
- How can I generate symbol (.pdb) files?
    A: This is done with the -D CMAKE_BUILD_TYPE flag. =Release, =Debug, =RelWithDebInfo, = MinSizeRel
    https://cmake.org/cmake/help/latest/variable/CMAKE_BUILD_TYPE.html
    Hmm, Visual Studio doesn't like MinSizeRel for some reason. Complains about build type being defined twice...

##########
# combining multiple cmake projects
# (or including non-cmake projects in a cmake project)
##########


cmake, how to make packages.
https://stackoverflow.com/questions/55635294/how-to-create-packages-with-cmake
https://stackoverflow.com/questions/33462209/making-a-cmake-library-accessible-by-other-cmake-packages-automatically

cmake package registry:
https://gitlab.kitware.com/cmake/community/-/wikis/doc/tutorials/Package-Registry

not sure I like the registry approach...
I would rather specify the location of the child cmake project when building the parent.

cmake docs on user package registry, system package registry
https://cmake.org/cmake/help/git-stage/manual/cmake-packages.7.html#id20
disabling package registry
https://cmake.org/cmake/help/git-stage/manual/cmake-packages.7.html#id23


https://stackoverflow.com/questions/15175318/cmake-how-to-build-external-projects-and-include-their-targets

multiple executables, and ExternalProject_Add?
https://stackoverflow.com/questions/33443164/cmake-share-library-with-multiple-executables
https://cmake.org/cmake/help/latest/module/ExternalProject.html#id1

another recommendation to use ExternalProject (Threading Building Blocks, TBB)
https://www.selectiveintellect.net/blog/2016/7/29/using-cmake-to-add-third-party-libraries-to-your-project-1
ExternalProject_Add, ExternalProject_Get_Property and ExternalProject_Add_Step
Are these instructions for non-cmake dependencies?

basic cmake, part 2: libraries (Jun 2018)
https://codingnest.com/basic-cmake-part-2/

how to set compiler-specific flags
https://codingnest.com/basic-cmake/

another recommendation for ExternalProject_Add (cmake by example)
recommended for including non-cmake projects
https://mirkokiefer.com/cmake-by-example-f95eb47d45b1
"If you don't specify commands like BUILD_COMMAND or INSTALL_COMMAND, CMake will look for a CMakeLists.txt in the external project and execute it."

can use add_library to add already built libraries.
https://mirkokiefer.com/cmake-by-example-f95eb47d45b1

question: why would i need to nest CMake projects? why not just download/build/install each separately?


##########
# misc
##########

components of a c++ project:

header file:
	.hpp
source file:
	.cpp
object file:
	.obj for 32-bit windows, .o for 64-bit windows, linux
static lib:
	.lib on windows, .a on linux
dynamic lib:
	.dll on windows, .so on linux
	Also called shared library.
	API is always in C?
import lib:
	.lib on windows (same as static lib), not required on linux?
	Thin static wrapper around dynamic library?
	Converts C++ call to C?
executable:
	.exe on windows, no extension on linux
program database:
	.pdb on windows, not available on linux?

for some of these, there are 32-bit and 64-bit variants, as well as release and debug variants.

the output of a project (files that are installed) usually go in 3 places:
/bin (executables)
/lib (static and dynamic libraries)
/include (header files required to incorporate the project into a larger c++ project)


I've picked up an idiom over time, don't know if it's attributable to anyone.
I like to structure my project source with a command line executable, and a separate library project with functionality. I think this is the pattern used by, for example, GDAL.
Consumers of the project may want to build and run command line util, or they may wish to call the c++ api.

I don't understand why generators are used for BUILD_INTERFACE, INSTALL_INTERFACE here:
https://cmake.org/cmake/help/latest/command/target_include_directories.html

cmake include_directories (old?) vs target_include_directories
https://stackoverflow.com/questions/31969547/what-is-the-difference-between-include-directories-and-target-include-directorie

"INTERFACE_INCLUDE_DIRECTORIES property of the target. This property is useful when another target uses target_link_libraries to link to the original target, as the linking target will have automatically those include directories added."
https://stackoverflow.com/a/40244458/2512141

I feel like this is important...
Two very different ways to structure a cmake project:
https://cliutils.gitlab.io/modern-cmake/chapters/basics/structure.html
https://rix0r.nl/blog/2015/08/13/cmake-guide/

I like the second approach better. It seems like putting each target in its own directory
(with a single accompanying CMakeLists.txt file)
is a more natural fit for how cmake is used.

daniel pfifer cmake introduction and best practices
https://www.slideshare.net/DanielPfeifer1/cmake-48475415

public vs private header files in cpp?
https://stackoverflow.com/questions/2272735/private-public-header-example

discussion on how to nest header files in a cpp project, with reply by herb sutter (2018):
https://github.com/isocpp/CppCoreGuidelines/issues/1259

heh.
"I prefer folder hierarchies matching namespace hierarchies"
https://www.reddit.com/r/cpp/comments/8qzepa/poll_c_project_layout/

cmake build tree (.../build) and source tree (.../build/..)
https://cmake.org/cmake/help/v3.17/manual/cmake.1.html

cmake hint: use "modern cmake" as a search term. it's apparently a well-recognized concept,
not sure when it took effect

proposal for a canonical c++ project structure (not sure who wrote this.)
http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2018/p1204r0.html
"If you need to separate public API headers/modules from implementation details, the convention is to place them into the details/ subdirectory."

the juggling act here is that we must manually align directory structure, header file include calls, and nested namespaces.

one goal which i think is worthwhile is to ensure that my development experience while building the project is the same as the experience of someone consuming the project.
that is, if consumer will #include <dd/foo/foo.hpp>, then I should not be doing #include <foo.hpp>

facebook's C++ library, uses CMakeLists.txt
https://github.com/facebook/folly

Ultimate CMake/C++ quick start
https://www.youtube.com/watch?v=YbgH7yat-Jo
https://github.com/lefticus/cpp_starter_project
https://crascit.com/professional-cmake/ $30 ebook

intro to cmake
https://www.youtube.com/watch?v=HPMvU64RUTY&t=552s

considerations when adopting a cmake structure:
- what would it look like if it were a header-only library?
- what would happen if you needed to divide a target into multiple subtargets?

https://www.reddit.com/r/cpp/comments/6m7sp6/cmake_and_c_whats_the_deal_with_installing/
"If you are using CPack you can get a .deb, .rpm, a windows installer or whatever thing they are using on mac"
a few folks saying that cmake install is not meant to install on your system, but rather build an installable package.

interesting... Facebook's folly library doesn't always keep namespaces synced with dir structure:
https://github.com/facebook/folly/blob/master/folly/ClockGettimeWrappers.h

should i distinguish somehow between include files that are header-only and include files that require a .cpp implementation?

hm, googletest uses cmake but doesn't follow the "stuttering" convention.
https://github.com/google/googletest
the repo has two projects, googlemock and googletest, and each one has an include directory.
That just makes better sense... The interface for a project should not live in a directory separate from the project itself.

and, just to keep things simple, let's keep namespaces synced with directory structure.
i think python packages do this implicitly.
ah, wait. binaries can't have main() inside a namespace.

https://www.slideshare.net/DanielPfeifer1/cmake-48475415
slide 24, "Make sure that all your projects can be built both standalone and as a subproject of another project"
NOTE: He said project, not library.
Slide 27, "Libraries can be STATIC, SHARED, MODULE, or INTERFACE"
slide 28, "Don't make libraries STATIC/SHARED unless they cannot be built otherwise."
always add namespaced aliases for libraries? add_library(my::foo ALIAS foo)
PRIVATE PUBLIC INTERFACE
avoid the link_libraries() command
avoid the link_directories() command
no need to call add_dependencies()
avoid the include_directories() command
avoid the add_definitions() command
target_include_directories(foo PUBLIC include PRIVATE src)
scripting mode/configure mode?
list all sources explicitly:
add_library(mylib main.cpp file1.cpp file2.cpp)

Really struggling to decide between keeping include files with the libraries themselves or in a top-level include directory.
I really, really dislike how messy the dir tree looks when each lib has its own includ structure.
I think you can make an argument for keeping public header files in a top-level include dir.
Public header files are contracts between components, and it could make sense that these contracts should be kept in a central place.

Discussion on C++ internal/external headers
http://www.cplusplus.com/forum/beginner/83755/
https://stackoverflow.com/questions/11063355/is-anyone-familiar-with-the-implementation-internal-header-ih
Also called private/public headers?
https://stackoverflow.com/questions/2272735/private-public-header-example
"You can declare all the interfaces and constants you want to expose to the library user in a separate header file (or a set of files) and put it into "inc" subdirectory - those headers will be "public". You will also use other header files to declare classes and stuff you don't want to expose since theay are implementation details - you will put those files into "src" subdirectory - those will be "private"."

Why put things in headers? Why put things in sources?
Code in headers cannot be compiled once to object files. It must be recompiled every time.
Code in sources cannot use templates. (?)
https://stackoverflow.com/questions/583255/c-code-in-header-files

To master a language, you must understand:
- The language.
- The standard library.
- Memory allocation/deallocation model.
- Concurrency model.
- The debugger.
- The package manager.
- Documentation generation.

to show all current cmake variables, go to build dir (dir with CMakeCache.txt) and run:
cmake -LAH
https://stackoverflow.com/questions/9298278/cmake-print-out-all-accessible-variables-in-a-script


guidelines for public/private headers.
https://stackoverflow.com/questions/44363595/cmake-dependency-conflict-with-same-header-file-names


using cmake to precompile headers?
https://cmake.org/cmake/help/git-stage/command/target_precompile_headers.html

boost unit test framework does not support testing binaries? only header, static lib, shared lib?
https://www.boost.org/doc/libs/1_73_0/libs/test/doc/html/boost_test/usage_variants.html


# Why might we want to use generator expressions when defining include locations?
# https://pabloariasal.github.io/2018/02/19/its-time-to-do-cmake-right/
# While building jsonutils, include/ is at /home/.../libjsonutils/include/, 
# but after installing our library, it will be under ${CMAKE_INSTALL_PREFIX}/include/. 
# Therefore, the location of this directory needs to be different depending on whether we are building or installing the library.