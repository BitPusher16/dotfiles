#!/bin/bash

# Right now the path to vim bak files is hard coded.
# We must either parameterize this or hard code the path where
# dotfiles is deployed.
# For now, we choose the latter.
SCRIPT_PATH="$( cd "$(dirname "$0")" ; pwd -P )"
#echo $SCRIPT_PATH
REQUIRED_PATH="$( cd ~/dotfiles ; pwd -P )"
#echo $REQUIRED_PATH

cd $SCRIPT_PATH
if [[ $SCRIPT_PATH == $REQUIRED_PATH ]]; then
	echo "running from the correct path"
else
	echo "not running from the correct path"
	exit
fi

echo ". ~/dotfiles/bashrc # dotfile_flag"                     >> ~/.bashrc
echo ". ~/dotfiles/profile # dotfile_flag"                    >> ~/.profile
echo ". ~/dotfiles/aliases # dotfile_flag"                    >> ~/.aliases
echo ". ~/dotfiles/aliases # dotfile_flag"                    >> ~/.bash_aliases
echo "source $HOME/dotfiles/screenrc # dotfile_flag"          >> ~/.screenrc
echo "source ~/dotfiles/vimrc \"dotfile_flag"                 >> ~/.vimrc
echo "source-file ~/dotfiles/tmux.conf # dotfile_flag"        >> ~/.tmux.conf
printf "[include] # dotfile_flag\n\tpath = ~/dotfiles/gitconfig # dotfile_flag" >> ~/.gitconfig

# The .swp location lives in dotfiles now.
#mkdir -p ~/.vimbak/undo ~/.vimbak/backup ~/.vimbak/swp
