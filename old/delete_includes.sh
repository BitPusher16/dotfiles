#!/bin/bash

sed --in-place=.bak '/dotfile_flag/d' ~/.bashrc
sed --in-place=.bak '/dotfile_flag/d' ~/.profile
sed --in-place=.bak '/dotfile_flag/d' ~/.aliases
sed --in-place=.bak '/dotfile_flag/d' ~/.bash_aliases
sed --in-place=.bak '/dotfile_flag/d' ~/.vimrc
sed --in-place=.bak '/dotfile_flag/d' ~/.screenrc
sed --in-place=.bak '/dotfile_flag/d' ~/.tmux.conf
sed --in-place=.bak '/dotfile_flag/d' ~/.gitconfig

rm -rf ~/.vimbak
